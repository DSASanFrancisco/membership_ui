import React from 'react'
import { expect } from 'chai'
import renderer from 'react-test-renderer'

import Loading from '../../../src/js/components/common/Loading'

describe('<Loading />', () => {
  it('renders', done => {
    const tree = renderer.create(<Loading />).toJSON()
    expect(tree).to.matchSnapshot()
    done()
  })
})
