import { ImMeeting } from '../client/MeetingClient'
import { ImEligibleMemberList } from '../client/MemberClient'

export const isGeneralMeeting = (meeting: ImMeeting | null): boolean => {
  if (meeting != null) {
    const labelMatches =
      Boolean(meeting.get('name').match(/General/)) ||
      Boolean(meeting.get('name').match(/Regular/)) ||
      Boolean(meeting.get('name').match(/Special/))
    return labelMatches && meeting.get('committee_id') == null
  } else {
    return false
  }
}

export const countEligibleAttendees = (
  attendees: ImEligibleMemberList
): number => {
  return attendees.filter(
    attendee =>
      attendee.hasIn(['eligibility', 'is_eligible']) &&
      attendee.getIn(['eligibility', 'is_eligible'])
  ).size
}

export const countProxyVotes = (attendees: ImEligibleMemberList): number => {
  return attendees.reduce((result, attendee) => {
    const isPersonallyEligible =
      attendee.hasIn(['eligibility', 'is_eligible']) &&
      attendee.getIn(['eligibility', 'is_eligible'])
    const numVotes = attendee.getIn(
      ['eligibility', 'num_votes'],
      // if num_votes was not passed, assume is_eligible counts as 1
      isPersonallyEligible ? 1 : 0
    )
    const expectedNumVotes = isPersonallyEligible ? 1 : 0
    const numProxyVotes = numVotes - expectedNumVotes
    return result + numProxyVotes
  }, 0)
}
