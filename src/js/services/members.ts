import { List, Set } from 'immutable'
import { ImMemberState } from '../redux/reducers/memberReducers'
import { ImMemberDetailed, ImMembership } from 'src/js/client/MemberClient'

export function isMemberLoaded(member: ImMemberState) {
  return member && member.getIn(['user', 'data'], List()) != null
}

export function isMemberFromDetails(member: ImMemberDetailed) {
  return member
    .get('roles', List())
    .some(
      role =>
        role.get('role') === 'member' && role.get('committee') === 'general'
    )
}

export function isAdmin(member: ImMemberState) {
  if (!member) {
    return false
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .some(
      role =>
        role.get('role') === 'admin' && role.get('committee') === 'general'
    )
}

export function isCommitteeAdmin(
  member: ImMemberState,
  committees: string | string[] = 'any'
) {
  if (!member || !committees) {
    return false
  }
  const isAdmin = role => role.get('role') === 'admin'
  const committeeSet =
    typeof committees === 'string' ? Set([committees]) : Set(committees)
  const filter =
    committees === 'any'
      ? isAdmin
      : role => isAdmin(role) && committeeSet.has(role.get('committee'))

  const memberRoles = member.getIn(['user', 'data', 'roles'], List())
  return memberRoles.some(filter)
}

export function getRelevantCommittees(member: ImMemberState) {
  if (!member) {
    return List()
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .filter(
      role => role.get('role') === 'admin' || role.get('role') === 'active'
    )
}

export function isMember(member: ImMemberState) {
  if (!member) {
    return false
  }
  return member
    .getIn(['user', 'data', 'roles'], List())
    .some(
      role =>
        role.get('role') === 'member' && role.get('committee') === 'general'
    )
}

export function isMembershipExpired(
  membership: ImMembership,
  now: Date = new Date()
) {
  return (membership.get('dues_paid_until') as Date) < now
}

export enum MembershipStatus {
  NotAMember = 'Not a Member',
  GoodStanding = 'Member - In Good Standing',
  LeavingStanding = 'Member - Leaving Standing',
  OutOfStanding = 'Member - Out of Standing',
  UnknownStanding = 'Member - Unknown Standing',
  Suspended = 'Member - Suspended',
  LeftChapter = 'Not a Member - Left Chapter'
}

export function mapMembershipStatus(member: ImMemberDetailed) {
  const status = member.getIn(['membership', 'status']) as string

  if (status == null) {
    return MembershipStatus.NotAMember
  }

  return mapMembershipStatusString(status)
}

export function mapMembershipStatusString(status: string) {
  switch (status) {
    case 'SUSPENDED':
      return MembershipStatus.Suspended
    case 'LEFT_CHAPTER':
      return MembershipStatus.LeftChapter
    case 'UNKNOWN':
      return MembershipStatus.UnknownStanding
    case 'OUT_OF_STANDING':
      return MembershipStatus.OutOfStanding
    case 'GOOD_STANDING':
      return MembershipStatus.GoodStanding
    case 'LEAVING_STANDING':
      return MembershipStatus.LeavingStanding
    default:
      return MembershipStatus.NotAMember
  }
}

const members = {
  isMemberLoaded,
  isAdmin,
  isCommitteeAdmin,
  getRelevantCommittees
}

export default members
