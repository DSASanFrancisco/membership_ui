// per https://stackoverflow.com/a/25075575
// this is meant to give a nice set of rounding methods that
// behave as expected

type Function1Ary = (a: any) => any
type FunctionPropertyNames<T> = {
  [K in keyof T]: T[K] extends Function1Ary ? K : never
}[keyof T]
type DecimalAdjustmentType = FunctionPropertyNames<Math>

/**
 * Decimal adjustment of a number.
 *
 * @param   {DecimalAdjustmentType}    type    The type of adjustment.
 * @param   {Number}    value   The number.
 * @param   {Integer}   exp     The exponent (the 10 logarithm of the adjustment base).
 * @returns {Number}            The adjusted value.
 */
function decimalAdjust(
  type: DecimalAdjustmentType,
  value: number,
  exp?: number
): number {
  // If the exp is undefined or zero...
  if (typeof exp === 'undefined' || +exp === 0) {
    return Math[type](value)
  }
  value = +value
  exp = +exp
  // If the value is not a number or the exp is not an integer...
  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
    return NaN
  }
  // Shift
  const split = value.toString().split('e')
  const shiftedValue = Math[type](
    +(split[0] + 'e' + (split[1] ? +split[1] - exp : -exp))
  )
  // Shift back
  const shiftedSplit = shiftedValue.toString().split('e')
  return +(
    shiftedSplit[0] +
    'e' +
    (shiftedSplit[1] ? +shiftedSplit[1] + exp : exp)
  )
}

// Decimal round
export const round10 = (value: number, exp?: number) => {
  return decimalAdjust('round', value, exp)
}

// Decimal floor
export const floor10 = (value: number, exp?: number) => {
  return decimalAdjust('floor', value, exp)
}

// Decimal ceil
export const ceil10 = (value: number, exp?: number) => {
  return decimalAdjust('ceil', value, exp)
}
