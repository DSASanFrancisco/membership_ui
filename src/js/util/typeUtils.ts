/**
 * Modify an existing type and override certain members with incompatible types.
 * Useful for forms that largely conform to API shape, but that need to start with null fields
 *
 * See https://stackoverflow.com/questions/41285211/overriding-interface-property-type-defined-in-typescript-d-ts-file
 */
export type Modify<T, R> = Omit<T, keyof R> & R

/**
 * Brand a primitive type to approximate nominal typing in TypeScript.
 *
 * This prevents mixing primitive types in use-cases where mixing could be dangerous.
 *
 * **Branded primitives only allow construction via an explicit cast.**
 *
 * @template T the primitive to brand
 * @template TBrand an identifier for the brand to prevent conflicts. Defaults to a unique symbol type
 *
 *
 * @example
 * type UserId = Brand<string, "user-id">
 * type UserToken = Brand<string, "user-token">
 * const a: UserToken = getUserToken()
 * const b: UserId = a  // error!
 * const c: UserToken = "some-user-token"  // error!
 * const d: UserToken = "some-user-token" as UserToken  // ok
 */
export type Brand<T, TBrand extends string> = T & { __brand: TBrand }

/**
 * "Flavor" a primitive type to allow for slightly more flexible nominal typing in TypeScript.
 *
 * This has the same benefits as branded types, but Flavors allow implicit
 * casting from the underlying primitive type.
 *
 * @template T the primitive to flavor
 * @template TFlavor an identifier for the flavor to prevent conflicts. Defaults to a unique symbol type
 *
 * @example
 * type UserId = Flavor<string, "user-id">
 * type UserToken = Flavor<string, "user-token">
 * const a: UserToken = getUserToken()
 * const b: UserId = a;  // error!
 * const c: UserToken = "some-user-token"  // ok
 * const d: UserToken = "some-user-token" as UserToken  // ok
 */
export type Flavor<T, TFlavor extends string> = T & { __brand?: TFlavor }
