import { Api, ApiClient, ApiStatus } from './ApiClient'
import { logError } from '../util/util'
import { fromJS, List, Map } from 'immutable'
import { TypedMap, FromJS } from 'src/js/util/typedMap'
import { Candidate } from 'src/js/client/CandidateClient'

export class ElectionClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async submitVote(params: ImVoteBallot): Promise<ImSubmitVoteResponse | null> {
    const response = await this.api
      .url(`/vote`)
      .post(params)
      .execute<ImSubmitVoteResponse>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getVote(
    electionId: number,
    ballotKey: string
  ): Promise<ImGetVoteResponse | null> {
    const response = await this.api
      .url(`/election/${electionId}/vote/${ballotKey}`)
      .get()
      .composeErrorHandler((handler, res) => {
        return res.error.status === 404 || handler(res)
      })
      .executeOr<ImGetVoteResponse, null>(null)

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getEligibleVoters(
    electionId: number
  ): Promise<ImEligibleVotersById | null> {
    const response = await this.api
      .url(`/election/eligible/list`, { params: { election_id: electionId } })
      .get()
      .execute<ImEligibleVotersById>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getElections(): Promise<ImElectionsList | null> {
    const response = await this.api
      .url('/election/list')
      .get()
      .execute<ImElectionsList>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  async getElection(
    electionId: number
  ): Promise<ImElectionDetailsResponse | null> {
    const result = await this.api
      .url(`/election`, { params: { id: electionId } })
      .get()
      .executeOr<ImElectionDetailsResponse, null>(null)

    if (result != null) {
      // add the id since it isn't returned from the db
      return fromJS(result.set('id', electionId))
    } else {
      return null
    }
  }

  async countResults(electionId: number): Promise<ImElectionCount | null> {
    const response = await this.api
      .url(`/election/count`, {
        params: {
          id: electionId,
          visualization: true
        }
      })
      .get()
      .execute<ImElectionCount>()

    if (response != null) {
      return response
    } else {
      return null
    }
  }

  submitPaperBallot(ballot: ImPaperBallotRequest, override = false) {
    const ballotPost = ballot.set('override', override)
    return this.api
      .url(`/vote/paper`)
      .composeErrorHandler((handler, { error }) => {
        if (error.status === 404) {
          logError(
            `Cannot submit unclaimed ballot #${ballot.get('ballot_key')}`
          )
        } else {
          logError(
            `Unexpected error when submitting ballot ${ballotPost}`,
            error
          )
        }
      })
      .post(ballotPost)
      .execute()
  }

  async submitTransition(
    electionId: number,
    transition: ElectionTransition
  ): Promise<ImSubmitElectionTransitionResponse> {
    const response = await this.api
      .url(`/election/${electionId}/state/${transition}`)
      .put()
      .execute<ImSubmitElectionTransitionResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error(
        'Got an empty response from the server when submitting an election transition'
      )
    }
  }

  async updateElection(
    electionId: number,
    attributes: ImUpdateElectionAttributes
  ): Promise<ImUpdateElectionResponse> {
    if (!electionId) {
      throw new Error(`Invalid election_id: ${electionId}`)
    }
    const response = await this.api
      .url(`/election/${electionId}`)
      .patch(attributes.set('election_id', electionId))
      .execute<ImUpdateElectionResponse>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got an empty response when updating an election')
    }
  }
}

export interface UpdateElectionAttributes extends ElectionDetailsResponse {
  /**
   * Duplicate of `election.id` property
   * TODO: doesn't seem to be used in backend code.
   * @deprecated
   */
  election_id: number
}
type ImUpdateElectionAttributes = FromJS<Partial<UpdateElectionAttributes>>

interface ElectionSponsor {
  id: string
  name: string
}
type ImElectionSponsors = List<TypedMap<ElectionSponsor>>

export type ElectionStatus = 'draft' | 'published' | 'final' | 'canceled'
type ElectionTransition = ElectionStatus[]

interface SubmitElectionTransitionResponse extends ApiStatus {
  election_status: ElectionStatus
  transitions: ElectionTransition
}
type ImSubmitElectionTransitionResponse = FromJS<
  SubmitElectionTransitionResponse
>

export interface ElectionResponse {
  id: number
  name: string
  status: ElectionStatus
  start_time: string
  end_time: string
  voting_begins_epoch_millis: number
  voting_ends_epoch_millis: number
}
export type ImElectionResponse = FromJS<ElectionResponse>
export type ImElectionsList = FromJS<ElectionResponse[]>

export interface ElectionDetailsResponse extends ElectionResponse {
  number_winners: number
  candidates: Candidate[]
  sponsors: ElectionSponsor[]
  transitions: ElectionTransition
  description: string
  description_img: string
  author: string
}
export type ImElectionDetailsResponse = FromJS<ElectionDetailsResponse>

type VoteTransferType = 'excess' | 'elimination'
export interface VoteCount {
  starting_votes: number
  votes_received: number
}
export type ImVoteCount = FromJS<VoteCount>

export interface VoteCountByCandidate {
  [cid: string]: VoteCount
}

interface ElectionRoundInformation {
  transfer_type: VoteTransferType
  transfer_source: string
  votes: VoteCountByCandidate
}
type ImElectionRoundInformation = FromJS<ElectionRoundInformation>

interface ElectionWinner {
  candidate: number
  round: number
}
type ImElectionWinner = FromJS<ElectionWinner>

interface CandidatesById {
  [id: string]: Candidate
}
export type ImCandidatesById = FromJS<CandidatesById>

export interface ElectionCount {
  ballot_count: number
  quota: number
  candidates: CandidatesById
  winners: ElectionWinner[]
  round_information: ElectionRoundInformation[]
}
export type ImElectionCount = FromJS<ElectionCount>

interface GetVoteResponse {
  election_id: number
  ballot_key: string
  rankings: number[]
}

export type ImGetVoteResponse = FromJS<GetVoteResponse>

export interface EligibleVoter {
  name: string
  email_address: string
}
export type ImEligibleVoter = FromJS<EligibleVoter>

interface EligibleVotersById {
  [memberId: string]: EligibleVoter
}
export type ImEligibleVotersById = FromJS<EligibleVotersById>

interface PaperBallotRequest {
  election_id: number
  ballot_key: string
  override?: boolean
  rankings: number[]
}
export type ImPaperBallotRequest = FromJS<PaperBallotRequest>

interface UpdateElectionResponse extends ApiStatus {
  election: ElectionResponse
}
type ImUpdateElectionResponse = FromJS<UpdateElectionResponse>

interface VoteBallot {
  election_id: number
  rankings: number[]
}
export type ImVoteBallot = FromJS<VoteBallot>

interface SubmitVoteResponse {
  ballot_id: string
}
export type ImSubmitVoteResponse = FromJS<SubmitVoteResponse>

export const Elections = new ElectionClient(Api)
