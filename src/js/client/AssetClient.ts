import {
  Api,
  ApiClient,
  ApiStatus,
  ApiStatusType,
  ImApiStatus
} from '../client/ApiClient'
import { fromJS, Map, List } from 'immutable'
import request from 'superagent'
import { FromJS, typedKeyBy } from 'src/js/util/typedMap'

export type ResolvedAssetContentType = 'image' | 'audio' | 'video'

export interface ResolvedAsset extends RawAsset {
  view_url: string
  upload_link?: string
}
export type ImResolvedAsset = FromJS<ResolvedAsset>

interface AssetsListResponse extends ApiStatus {
  data: ResolvedAsset[]
}
type ImAssetsListResponse = FromJS<AssetsListResponse>

export interface AssetsById {
  [id: number]: ResolvedAsset
}
export type ImAssetsById = Map<number, ImResolvedAsset>

export type AssetType = 'EXTERNAL_URL' | 'PRESIGNED_URL'

export interface RawAsset {
  id: number
  title: string
  asset_type: AssetType | null
  last_updated: string
  content_type: string
  external_url?: string
  relative_path?: string
  labels: string[]
}
export type ImRawAsset = FromJS<RawAsset>

interface GetAssetResponse extends ApiStatus {
  data: ResolvedAsset
}
type ImGetAssetResponse = FromJS<GetAssetResponse>

interface GetUrlDataResponse {
  url: string
}

interface GetUrlResponse extends ApiStatus {
  data: GetUrlDataResponse
}
type ImGetUrlResponse = FromJS<GetUrlResponse>

export class AssetClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async search({ labels }): Promise<ImAssetsById | null> {
    const result = await this.api
      .url(`/assets`)
      .query({ labels })
      .get()
      .execute<ImAssetsListResponse>()

    if (result != null) {
      return typedKeyBy(result.get('data', List()), asset => asset.get('id'))
    } else {
      return null
    }
  }

  async uploadAndCreateAsset(
    asset: ImRawAsset,
    file: File
  ): Promise<ImResolvedAsset> {
    const relativePath = asset.get('relative_path')

    if (relativePath != null) {
      const uploadUrl = await this.getUploadUrl(relativePath)

      if (uploadUrl != null) {
        await this.uploadContent(file, uploadUrl)
        return await this.create(asset)
      } else {
        throw new Error('Invalid upload url')
      }
    } else {
      throw new Error('Invalid relative path')
    }
  }

  async getUploadUrl(relativePath: string): Promise<string | null> {
    const request = this.api
      .url(`/assets/upload_url`)
      .query({ relative_path: relativePath })
      .get()
    const res = await request.execute<ImGetUrlResponse>()

    if (res != null) {
      return res.get('data').get('url')
    } else {
      return null
    }
  }

  async getViewUrl(relativePath: string): Promise<string | null> {
    const request = this.api
      .url(`/assets/view_url`)
      .query({ relative_path: relativePath })
      .get()
    const res = await request.execute<ImGetUrlResponse>()

    if (res != null) {
      return res.get('data').get('url')
    } else {
      throw new Error('Server responded with bad view URL')
    }
  }

  async uploadContent(content: File, uploadUrl: string): Promise<ApiStatus> {
    // TODO: Use this.api instead of the underlying library
    return await request
      .put(uploadUrl)
      .set('Content-Type', 'application/octet-stream')
      .send(content)
      .then(response => response.body as ApiStatus)
  }

  async create(asset: ImRawAsset): Promise<ImResolvedAsset> {
    const request = this.api.url(`/assets`).put(asset)
    const updatedAsset = await request.execute<ImGetAssetResponse>()

    if (updatedAsset != null) {
      return updatedAsset.get('data')
    } else {
      throw new Error('Server responded with bad updated asset')
    }
  }

  async deleteById(assetId: number): Promise<ImApiStatus> {
    const response = await this.api
      .url(`/asset/${assetId}`)
      .remove()
      .execute<ImApiStatus>()

    if (response != null) {
      return response
    } else {
      throw new Error('Got an empty response from the server while deleting')
    }
  }
}

export const Assets = new AssetClient(Api)
export default Assets
