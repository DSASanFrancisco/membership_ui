import { Api, ApiClient } from '../client/ApiClient'
import { List, fromJS } from 'immutable'
import { FromJS } from '../util/typedMap'

export interface EmailTopic {
  id: number
  name: string
}

type EmailTopicListResponse = EmailTopic[]
type ImEmailTopicListResponse = FromJS<EmailTopicListResponse>

export interface EmailTopicById {
  [id: number]: EmailTopic
}

export class EmailTopicClient {
  api: ApiClient

  constructor(api: ApiClient) {
    this.api = api
  }

  async all(): Promise<EmailTopicListResponse> {
    const response = await this.api
      .url('/email_topics')
      .get()
      .executeOr<ImEmailTopicListResponse>(List())

    // TODO: Remove once the base API client isn't tied to Immutable.js anymore
    return response.toJS()
  }

  async send(
    topicId: number,
    templateId: number,
    address: string
  ): Promise<void> {
    const response = await this.api
      .url('/email_topics/send')
      .post({
        topic_id: topicId,
        template_id: templateId,
        sending_address: address
      })
      .execute()

    if (response == null) {
      throw new Error('Got blank response from server when sending email')
    }
  }
}

export const EmailTopics = new EmailTopicClient(Api)
