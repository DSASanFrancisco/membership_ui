interface DisableFeatures {
  ELECTIONS: boolean
}

export type DisableFeatureKey = keyof DisableFeatures
