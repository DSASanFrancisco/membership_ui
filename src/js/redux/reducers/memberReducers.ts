import { List, Map, fromJS } from 'immutable'
import { fetchDataHandler } from './decorators'
import { FETCH_DATA_SUCCESS } from '../actions/fetchDataActions'
import { Reducer } from 'react'
import { TODO } from '../../typeMigrationShims'
import { AnyAction } from 'redux'
import { MemberDetailed } from 'src/js/client/MemberClient'
import { FromJS } from 'src/js/util/typedMap'

interface MemberState {
  isAdmin: boolean
  user: {
    data: MemberDetailed | null
    loading: boolean
    err: TODO | null
  }
}

export type ImMemberState = FromJS<MemberState>
export type MemberAction = AnyAction

export const MEMBER_STORE = 'member'

export const INITIAL_STATE: ImMemberState = fromJS({
  isAdmin: false,
  user: {
    data: null as MemberDetailed | null,
    loading: false,
    err: null
  }
})

const member: Reducer<ImMemberState, MemberAction> = (
  state = INITIAL_STATE,
  action
) => {
  if (action.store !== MEMBER_STORE) {
    return state
  }

  switch (action.type) {
    case FETCH_DATA_SUCCESS:
      return state.set(
        'isAdmin',
        state
          .getIn(['user', 'data', 'roles'], List())
          .some(
            r => r.get('role') === 'admin' && r.get('committee') === 'general'
          )
      )

    default:
      return state
  }
}

export default fetchDataHandler<ImMemberState, MemberAction>(
  member,
  MEMBER_STORE
)
