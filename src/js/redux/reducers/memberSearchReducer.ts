import { List, Map, fromJS } from 'immutable'
import {
  MEMBER_SEARCH_INPUT_UPDATED,
  MEMBER_SEARCH_INPUT_CLEARED,
  MEMBER_SEARCH_QUERY_REQUESTED,
  MEMBER_SEARCH_QUERY_SUCCEEDED,
  MEMBER_SEARCH_LOAD_MORE_REQUESTED,
  MEMBER_SEARCH_LOAD_MORE_SUCCEEDED
} from '../constants/actionTypes'
import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { typedMerge, FromJS } from '../../util/typedMap'
import {
  ImEligibleMemberList,
  ImEligibleMemberListEntry
} from 'src/js/client/MemberClient'

interface MemberSearchState {
  query: string
  results: ImEligibleMemberList
  hasMoreResults: boolean
  isLoading: boolean
  cursor: number
}
export type ImMemberSearchState = FromJS<MemberSearchState>

const reducer: Reducer<ImMemberSearchState, AnyAction> = (
  state = fromJS<MemberSearchState>({
    query: '',
    results: List<ImEligibleMemberListEntry>(),
    hasMoreResults: false,
    isLoading: false,
    cursor: 0
  }),
  action
) => {
  switch (action.type) {
    case MEMBER_SEARCH_INPUT_UPDATED:
      if (action.payload === '') {
        return typedMerge(state, {
          query: '',
          results: List<ImEligibleMemberListEntry>(),
          hasMoreResults: false
        })
      }
      return state.set('query', action.payload)
    case MEMBER_SEARCH_INPUT_CLEARED:
      return typedMerge(state, {
        query: '',
        results: List<ImEligibleMemberListEntry>(),
        hasMoreResults: false
      })
    case MEMBER_SEARCH_QUERY_REQUESTED:
      return state.set('isLoading', true)
    case MEMBER_SEARCH_LOAD_MORE_REQUESTED:
      return state.set('isLoading', true)
    case MEMBER_SEARCH_QUERY_SUCCEEDED:
      return typedMerge(state, {
        isLoading: false,
        hasMoreResults: action.payload.get('has_more'),
        results: action.payload.get('members'),
        cursor: action.payload.get('cursor')
      })
    case MEMBER_SEARCH_LOAD_MORE_SUCCEEDED:
      const results = List(
        state.get('results').concat(action.payload.get('members'))
      )

      return typedMerge(state, {
        isLoading: false,
        hasMoreResults: action.payload.get('has_more'),
        results,
        cursor: action.payload.get('cursor')
      })
    default:
      return state
  }
}

export default reducer
