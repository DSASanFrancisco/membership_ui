import { AnyAction } from 'redux'
import { Reducer } from 'react'
import produce from 'immer'
import _ from 'lodash'

import * as types from './../constants/actionTypes'
import { EmailTopicById } from 'src/js/client/EmailTopicClient'
import { EmailTopicActions } from '../actions/emailTopicActions'

export interface TopicsState {
  readonly byId: EmailTopicById
}

export type TopicsAction = AnyAction

const INITIAL_STATE: TopicsState = {
  byId: {}
}

const topics: Reducer<TopicsState, EmailTopicActions> = (
  state = INITIAL_STATE,
  action
) => {
  switch (action.type) {
    case types.FETCH_EMAIL_TOPICS:
      return produce(state, draft => {
        draft.byId = _.keyBy(action.payload, 'id')
      })
    default:
      return state
  }
}

export default topics
