import { ASSETS } from '../constants/actionTypes'
import { fromJS, Map } from 'immutable'
import { Reducer } from 'react'
import { AnyAction } from 'redux'
import { AssetsById } from 'src/js/client/AssetClient'
import { FromJS, typedMerge } from 'src/js/util/typedMap'

interface AssetsState {
  byId: AssetsById
}
export type ImAssetsState = FromJS<AssetsState>
export type AssetsAction = AnyAction

const INITIAL_STATE: ImAssetsState = fromJS({
  byId: {}
})

const assets: Reducer<ImAssetsState, AssetsAction> = (
  state = INITIAL_STATE,
  action
) => {
  const currentAssets = state.get('byId')
  switch (action.type) {
    case ASSETS.UPDATE_LIST:
      return state.set('byId', typedMerge(currentAssets, action.payload))
    case ASSETS.DELETE_MULTIPLE:
      let assetsRemoved = currentAssets
      action.payload.forEach(a => {
        const assetId = a.get('id')
        if (assetId) {
          assetsRemoved = assetsRemoved.remove(assetId)
        }
      })
      return state.set('byId', assetsRemoved)
    default:
      return state
  }
}

export default assets
