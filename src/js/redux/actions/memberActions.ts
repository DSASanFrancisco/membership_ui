import { fetchData } from './fetchDataActions'
import { membershipApi } from '../../services/membership'
import { logError } from '../../util/util'
import { MEMBER_STORE as store } from '../reducers/memberReducers'
import { USE_AUTH } from '../../config'
import { searchMembers } from './memberSearchActions'
import {
  MemberClient,
  Members,
  UpdateMemberAttributes,
  ImCreateMemberForm
} from '../../client/MemberClient'
import { IMPORT_MEMBERS, MEMBERS_CREATED } from '../constants/actionTypes'
import { fromJS } from 'immutable'
import { FETCH_DATA_SUCCESS } from '../../redux/actions/fetchDataActions'
import { Dispatch } from 'redux'
import {
  TODOThunkAction,
  TODOAsyncThunkAction
} from 'src/js/typeMigrationShims'

export function fetchMember(): TODOThunkAction {
  return dispatch => {
    const user = localStorage.user
    if (!USE_AUTH || user !== null) {
      dispatch(
        fetchData({
          apiService: membershipApi,
          route: '/member/details',
          store,
          keyPath: ['user']
        })
      )
    }
  }
}

export function createMember(
  newMember: ImCreateMemberForm
): TODOAsyncThunkAction {
  return async (dispatch, getState, extra) => {
    const state = getState()
    const client = new MemberClient(state.client)
    const body = await client.create(newMember)
    const m = body.getIn(['data', 'member'])
    const member = {
      id: m.get('id'),
      email: m.getIn(['info', 'email_address'], ''),
      name:
        m.getIn(['info', 'first_name'], '') +
        ' ' +
        m.getIn(['info', 'last_name'], '')
    }
    dispatch({
      type: MEMBERS_CREATED,
      payload: fromJS({
        member
      })
    })
    // refresh the search results
    searchMembers(state.memberSearch.get('query'))(dispatch, getState, extra)
  }
}

export function updateMember(
  attributes: Partial<UpdateMemberAttributes>
): TODOThunkAction {
  return (dispatch: Dispatch) => {
    Members.update(attributes)
      .then(body => {
        dispatch({
          store,
          type: FETCH_DATA_SUCCESS,
          keyPath: ['user'],
          data: body.get('data')
        })
      })
      .catch(err => {
        logError(err.toString(), err)
      })
  }
}

export function importRoster(file: File): TODOThunkAction {
  return (dispatch: Dispatch) => {
    dispatch({ type: IMPORT_MEMBERS.SUBMIT })
    Members.importRoster(file)
      .then(data => {
        dispatch({
          type: IMPORT_MEMBERS.SUCCESS,
          payload: data.get('data')
        })
      })
      .catch(err => {
        dispatch({
          type: IMPORT_MEMBERS.FAILED
        })
        logError(err.toString(), err)
      })
  }
}
