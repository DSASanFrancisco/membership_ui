import { ThunkDispatch } from 'redux-thunk'

import * as types from './../constants/actionTypes'
import { EmailTopics, EmailTopic } from '../../client/EmailTopicClient'
import { logError, showNotification } from 'src/js/util/util'
import { TopicsState } from '../reducers/emailTopics'

export interface FetchEmailTopics {
  type: typeof types.FETCH_EMAIL_TOPICS
  payload: EmailTopic[]
}

export interface SendEmailTemplateSucceeded {
  type: typeof types.SEND_EMAIL_TEMPLATE_SUCCEEDED
}

export interface SendEmailTemplateFailed {
  type: typeof types.SEND_EMAIL_TEMPLATE_FAILED
  payload: Error
}

export type EmailTopicActions =
  | FetchEmailTopics
  | SendEmailTemplateSucceeded
  | SendEmailTemplateFailed

export const fetchEmailTopics = () => async (
  dispatch: ThunkDispatch<TopicsState, unknown, FetchEmailTopics>
): Promise<void> => {
  const topics = await EmailTopics.all()
  dispatch({
    type: types.FETCH_EMAIL_TOPICS,
    payload: topics
  })
}

export const sendEmailTemplate = (
  topicId: number,
  templateId: number,
  address: string
) => async (
  dispatch: ThunkDispatch<
    TopicsState,
    unknown,
    SendEmailTemplateSucceeded | SendEmailTemplateFailed
  >
): Promise<void> => {
  try {
    EmailTopics.send(topicId, templateId, address)
    dispatch({ type: types.SEND_EMAIL_TEMPLATE_SUCCEEDED })
  } catch (error) {
    logError(JSON.stringify(error))
    dispatch({
      type: types.SEND_EMAIL_TEMPLATE_FAILED,
      payload: error
    })
  } finally {
    showNotification('Sent!', 'Email template sent!')
  }
}
