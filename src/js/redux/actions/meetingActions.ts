import { deleteAsset } from './assetActions'
import { ASSETS, MEETINGS } from '../constants/actionTypes'
import { Meetings, MeetingClient, ImMeeting } from '../../client/MeetingClient'
import {
  AssetClient,
  ImRawAsset,
  AssetType,
  ImResolvedAsset
} from '../../client/AssetClient'
import { logError } from '../../util/util'
import { Map } from 'immutable'
import { EXTERNAL_URL, PRESIGNED_URL } from 'src/js/models/assets'
import { TODOAsyncThunkAction } from 'src/js/typeMigrationShims'
import { ImAssetForm } from 'src/js/components/asset/EditableAsset'

export const MeetingCodeActions = Object.freeze({
  AUTOGENERATE: 'autogenerate',
  REMOVE: 'remote',
  SET: 'set'
})

export function fetchAllMeetings(): TODOAsyncThunkAction {
  return async dispatch => {
    try {
      const meetings = await Meetings.all()
      dispatch({
        type: MEETINGS.UPDATE_LIST,
        payload: meetings
      })
    } catch (err) {
      logError(JSON.stringify(err))
    }
  }
}

export function createMeeting(meeting: ImMeeting): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    dispatch({
      type: MEETINGS.CREATE.SUBMIT,
      payload: meeting
    })
    try {
      const state = getState()
      const meetings = new MeetingClient(state.client)
      const result = await meetings.create(meeting)
      const created = meetings.parseMeetingDates(result.get('meeting'))
      dispatch({
        type: MEETINGS.CREATE.SUCCESS,
        payload: created
      })
    } catch (error) {
      dispatch({
        type: MEETINGS.CREATE.FAILED,
        error,
        payload: meeting
      })
    }
  }
}

export function claimMeetingCode(
  meetingId: number,
  code: string
): TODOAsyncThunkAction {
  return async (dispatch, getState) => {
    const payload = {
      meetingId,
      code
    }
    dispatch({
      type: MEETINGS.CODE.SUBMIT,
      payload
    })
    const state = getState()
    const meetings = new MeetingClient(state.client)
    const promise =
      code === MeetingCodeActions.AUTOGENERATE
        ? meetings.autogenerateMeetingCode(meetingId)
        : meetings.setMeetingCode(meetingId, code)
    try {
      const result = await promise
      dispatch({
        type: MEETINGS.CODE.SUCCESS,
        payload: {
          meetingId,
          code: result.getIn(['meeting', 'code'])
        }
      })
    } catch (error) {
      dispatch({
        type: MEETINGS.CODE.FAILED,
        error,
        payload
      })
    }
  }
}

export function deleteMeetingAsset(assetId: number): TODOAsyncThunkAction {
  return async (dispatch, getState, extra) => {
    await deleteAsset({ id: assetId })(dispatch, getState, extra) // throws exception on failure
    const state = getState()
    dispatch({
      type: ASSETS.UPDATE_LIST,
      payload: state.assets.deleteIn(['byId', assetId]).get('byId') || Map()
    })
  }
}

export function saveMeetingAsset(
  asset: ImAssetForm,
  file: File
): TODOAsyncThunkAction<ImResolvedAsset | null> {
  return async (dispatch, getState) => {
    dispatch({
      type: MEETINGS.ADD_ASSET.SUBMIT,
      payload: asset
    })
    const state = getState()
    const client = new AssetClient(state.client)
    const operation = pickMeetingAssetOperation(asset, file, client)

    try {
      const created = await operation(asset, file)
      const update = Map().set(created.get('id'), created)
      dispatch({
        type: ASSETS.UPDATE_LIST,
        payload: update
      })
      dispatch({
        type: MEETINGS.ADD_ASSET.SUCCESS
      })
      return created
    } catch (ex) {
      dispatch({
        type: MEETINGS.ADD_ASSET.FAILED,
        payload: ex
      })
    }
    return null
  }
}

function pickMeetingAssetOperation(
  asset: ImAssetForm,
  file: File,
  client: AssetClient
): (asset: ImAssetForm, file: File) => Promise<ImResolvedAsset> {
  const assetType = asset.get('asset_type')

  if (assetType === EXTERNAL_URL) {
    return client.create.bind(client)
  } else if (assetType === PRESIGNED_URL) {
    if (file) {
      return client.uploadAndCreateAsset.bind(client)
    } else {
      return client.create.bind(client)
    }
  } else {
    throw new Error(`Unrecognized asset_type: '${assetType}'`)
  }
}
