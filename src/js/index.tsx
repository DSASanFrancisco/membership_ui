import 'core-js/stable'
import 'regenerator-runtime/runtime'

import React from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import configureStore from './redux/store/configureStore'
import { serviceLocator } from './util/serviceLocator'
import Root from './Root'
import 'focus-visible'
import './styling/styles'

const store = configureStore()
serviceLocator.store = store
const history = syncHistoryWithStore(browserHistory, store)
const rootElement = document.getElementById('root')

ReactDOM.render(<Root store={store} history={history} />, rootElement)
