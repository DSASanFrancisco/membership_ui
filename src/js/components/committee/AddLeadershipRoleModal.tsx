import React, { useState } from 'react'
import { Modal, Form } from 'react-bootstrap'
import { useQuery, useMutation } from 'react-query'
import CommitteeLeadership, {
  ImCommitteeLeader,
  AddLeadershipRoleArgs
} from 'src/js/client/CommitteeLeadersClient'
import FieldGroup from '../common/FieldGroup'
import { List } from 'immutable'

interface AddLeadershipRoleProps {
  committeeId: number
  onComplete: (ImCommitteeLeader) => void
  onCancel: () => void
}

const AddLeadershipRole: React.FC<AddLeadershipRoleProps> = ({
  committeeId,
  onComplete,
  onCancel
}) => {
  const { mutate: submitInner } = useMutation<
    ImCommitteeLeader,
    Error | null,
    AddLeadershipRoleArgs
  >(args => CommitteeLeadership.addLeadershipRole(args))
  const roleTitlesQuery = useQuery('roleTitles', () =>
    CommitteeLeadership.listRoleTitles()
  )
  const roleTitleOptions = (roleTitlesQuery.data || List())
    .push('Add Custom Role')
    .toJSON()
  const [selectedRoleTitle, setSelectedRoleTitleInner] = useState('')
  const [showCustomRole, setShowCustomRole] = useState(false)
  const setSelectedRoleTitle = (roleTitle: string) => {
    setShowCustomRole(roleTitle === 'Add Custom Role')
    setSelectedRoleTitleInner(roleTitle)
  }
  const [customRoleTitle, setCustomRoleTitle] = useState('')
  const [termLimitMonths, setTermLimitMonths] = useState(0)
  const submitEnabled =
    selectedRoleTitle && (!showCustomRole || customRoleTitle)
  const submit = async () => {
    const result = await submitInner({
      committeeId,
      roleTitle: showCustomRole ? customRoleTitle : selectedRoleTitle,
      termLimitMonths
    })
    onComplete(result)
  }

  return (
    <Modal.Body>
      <p>Add a new leadership&nbsp;role</p>
      <Form onSubmit={e => e.preventDefault()}>
        <FieldGroup
          formKey="role_title"
          label="Role Title"
          componentClass="select"
          options={roleTitleOptions}
          placeholder="Select a role title"
          value={selectedRoleTitle}
          onFormValueChange={(k, v) => setSelectedRoleTitle(v)}
          required
        />
        {showCustomRole ? (
          <FieldGroup
            formKey="custom_role_title"
            componentClass="input"
            type="text"
            label="Custom Role Title"
            value={customRoleTitle}
            onFormValueChange={(k, v) => setCustomRoleTitle(v)}
            required
          />
        ) : null}
        <FieldGroup
          formKey="term_limit_months"
          componentClass="input"
          type="number"
          step={1}
          min={0}
          label="Term Limit in Months (optional)"
          value={termLimitMonths}
          onFormValueChange={(k, v) => setTermLimitMonths(v)}
          required
        />
        <div>
          <button type="button" onClick={submit} disabled={!submitEnabled}>
            Submit
          </button>{' '}
          <button type="button" onClick={onCancel}>
            Cancel
          </button>
        </div>
      </Form>
    </Modal.Body>
  )
}

export default AddLeadershipRole
