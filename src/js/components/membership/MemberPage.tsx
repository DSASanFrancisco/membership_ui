import dateFormat from 'dateformat'
import { fromJS } from 'immutable'
import { capitalize } from 'lodash/fp'
import React, { Component } from 'react'
import {
  Button,
  Col,
  Container,
  Form,
  Modal,
  OverlayTrigger,
  Row,
  Tab,
  Tabs,
  Tooltip
} from 'react-bootstrap'
import { Helmet } from 'react-helmet'
import { FiTrash, FiHelpCircle, FiXCircle } from 'react-icons/fi'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import EditMemberModal from 'src/js/components/membership/EditMemberModal'
import MemberNotesTextarea from 'src/js/components/membership/MemberNotesTextarea'
import { c, USE_ELECTIONS } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import {
  ImMemberDetailed,
  ImMemberRole,
  ImMembership,
  MemberClient,
  Members,
  UpgradeMemberResponseData
} from '../../client/MemberClient'
import {
  mapMembershipStatus,
  MembershipStatus as MS
} from '../../services/members'
import { fetchElections } from '../../redux/actions/electionActions'
import { membershipApi } from '../../services/membership'
import { HTTP_POST, logError, showNotification } from '../../util/util'
import AddEligibleVoter from '../admin/AddEligibleVoter'
import AddMeeting from '../admin/AddMeeting'
import AddRole from '../admin/AddRole'
import Loading from '../common/Loading'
import PageHeading from '../common/PageHeading'
import EligibleVotes from './EligibleVotes'
import MergeMemberModal from './MergeMemberModal'

dateFormat.masks.dsaElection = "dddd, mmmm dS 'at' h:MM TT"

interface MemberPageParamProps {
  memberId: string
}
interface MemberPageRouteParamProps {}
type MemberPageProps = MemberPageStateProps &
  MemberPageDispatchProps &
  RouteComponentProps<MemberPageParamProps, MemberPageRouteParamProps>

type MemberPageTabKey = 'committees' | 'meetings' | 'elections' | 'notes'

interface MemberPageState {
  member: ImMemberDetailed | null
  inSubmission: boolean
  meetingShortId: string
  tabKey: MemberPageTabKey
  showRemoveRolePrompt: string | null
  showEditInfoModal: boolean
  showMergeMemberModal: boolean
}

const unknownTooltip = props => (
  <Tooltip id="overridden-tooltip" {...props}>
    We've confirmed this individual is registered with DSA National; however,
    national does not send us their membership record as part of our weekly
    imports. This typically occurs because an individual has a mailing address
    outside of SF zipcodes and their record is probably being sent to another
    Bay Area chapter.
  </Tooltip>
)

const overriddenTooltip = props => (
  <Tooltip id="overridden-tooltip" {...props}>
    Every week we import our member roster from National and update member dues.
    It takes a few weeks between when a member renews their dues and when we are
    notified, so admins can override the dues paid until field. This will be
    reset the next time this member's dues are updated!
  </Tooltip>
)

class MemberPage extends Component<MemberPageProps, MemberPageState> {
  constructor(props) {
    super(props)
    this.state = {
      member: null,
      inSubmission: false,
      meetingShortId: '',
      tabKey: 'committees',
      showRemoveRolePrompt: null,
      showEditInfoModal: false,
      showMergeMemberModal: false
    }
  }

  componentDidMount() {
    this.fetchMemberData()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.fetchMemberData()
    }
  }

  setKey = (k: MemberPageTabKey) => {
    this.setState({ tabKey: k })
  }

  showRemoveRolePrompt = (roleKey: string) => {
    this.setState({ showRemoveRolePrompt: roleKey })
  }

  hideRemoveRolePrompt = () => {
    this.setState({ showRemoveRolePrompt: null })
  }

  showMergeMemberModal = () => {
    this.setState({ showMergeMemberModal: true })
  }

  hideMergeMemberModal = () => {
    this.setState({ showMergeMemberModal: false })
  }

  showEditInfoModal = () => {
    this.setState({ showEditInfoModal: true })
  }

  hideEditInfoModal = () => {
    this.setState({ showEditInfoModal: false })
  }

  handleSaveComplete = () => {
    this.fetchMemberData()
  }

  upgradeToMember = async () => {
    const members = new MemberClient(this.props.client)
    const result = await members.upgradeToMember(
      parseInt(this.props.params.memberId, 10)
    )
    if (result) {
      const data: UpgradeMemberResponseData = result.get('data').toJS()
      const parts: string[] = []
      if (data.auth0_account_created) {
        parts.push('Auth0 account created')
      }
      if (data.email_sent) {
        parts.push('verify email sent')
      }
      if (data.role_created) {
        parts.push('member role created')
      }
      const details = parts.join(', ') || 'No changes necessary'
      showNotification('Upgrade to member succeeded', details)
    }
    await this.fetchMemberData()
  }

  suspendMember = async () => {
    const client = new MemberClient(this.props.client)
    const result = await client.suspendMember(
      parseInt(this.props.params.memberId, 10)
    )
    await this.fetchMemberData()
  }

  exitMember = async () => {
    const client = new MemberClient(this.props.client)
    const result = await client.exitMember(
      parseInt(this.props.params.memberId, 10)
    )
    await this.fetchMemberData()
  }

  cancelDuesOverride = async () => {
    const client = new MemberClient(this.props.client)
    await client
      .cancelDuesOverride(parseInt(this.props.params.memberId, 10))
      .then(function() {
        showNotification('Canceled dues override', '')
      })
    await this.fetchMemberData()
  }

  private getNameFromMember(memberData: ImMemberDetailed) {
    return `${memberData.getIn(['info', 'first_name'])} ${memberData.getIn([
      'info',
      'last_name'
    ])}`
  }

  render() {
    if (this.state.member === null) {
      return <Loading />
    }

    return (
      <Container className="admin-page member-page">
        <Row>
          <Col md={12}>
            <PageHeading level={1}>Manage member info</PageHeading>
          </Col>
        </Row>
        <Row>
          <Col md={4}>{this.renderMemberInfo()}</Col>
          <Col md={8}>
            <Tabs
              id="member-page-tabs"
              activeKey={this.state.tabKey}
              onSelect={k => k != null && this.setKey(k as MemberPageTabKey)}
            >
              <Tab
                eventKey="committees"
                title={`${capitalize(c('GROUP_NAME_PLURAL'))} and roles`}
              >
                {this.renderCommittees()}
              </Tab>
              <Tab eventKey="meetings" title="Meetings">
                {this.renderMeetings()}
              </Tab>
              <Tab eventKey="notes" title="Notes">
                {this.renderNotes()}
              </Tab>
              {USE_ELECTIONS && (
                <Tab eventKey="elections" title="Elections">
                  {USE_ELECTIONS && this.renderEligibleVoters()}
                </Tab>
              )}
            </Tabs>
          </Col>
        </Row>
      </Container>
    )
  }

  renderMemberInfo() {
    const memberData = this.state.member

    if (memberData != null) {
      const doNotEmail = memberData.get('do_not_email')
      const doNotCall = memberData.get('do_not_call')
      const name = this.getNameFromMember(memberData)
      const membership = memberData.get('membership') as
        | ImMembership
        | undefined
      const info = memberData.get('info')

      return (
        <section className="member-info-section">
          <Helmet>
            <title>{name}</title>
          </Helmet>
          <PageHeading level={2}>{name}</PageHeading>
          <dl className="member-info">
            <div className="info-email-address">
              <dt>Email address</dt>
              <dd>{info.get('email_address')}</dd>
            </div>
            <div className="info-eligibility">
              <dt>Eligibility Status</dt>
              <dd>
                <EligibilityStatus member={memberData} />
              </dd>
            </div>
            <div className="info-membership">
              <dt>Membership Status</dt>
              <dd>
                <MembershipStatus member={memberData} />
              </dd>
            </div>
            <div className="info-membership">
              <dt>Dues Status</dt>
              <dd>{this.renderDuesStatus()}</dd>
            </div>
            <div className="info-email-optout info-comms-optout">
              <dt>Okay to email?</dt>
              <dd>
                {doNotEmail ? (
                  <span className="comms-opt-out">No, do not email</span>
                ) : (
                  <span className="comms-okay">Yes, email okay</span>
                )}
              </dd>
            </div>
            <div className="info-phone-optout info-comms-optout">
              <dt>Okay to call?</dt>
              <dd>
                {doNotCall ? (
                  <span className="comms-opt-out">No, do not call</span>
                ) : (
                  <span className="comms-okay">Yes, calls okay</span>
                )}
              </dd>
            </div>
            {info.get('biography') != null && (
              <div className="info-biography">
                <dt>Biography</dt>
                <dd>{info.get('biography')}</dd>
              </div>
            )}
          </dl>
          <div className="mt-2 form-group">
            <Button onClick={this.showEditInfoModal} variant="primary">
              Edit info...
            </Button>
          </div>
          <div className="mt-2 form-group">
            <Button onClick={this.upgradeToMember}>Upgrade to Member</Button>
          </div>
          <div className="mt-2 form-group">
            <Button onClick={this.suspendMember}>Suspend Member</Button>
          </div>
          <div className="mt-2 form-group">
            <Button onClick={this.exitMember}>Exit Member</Button>
          </div>
          <div className="form-group">
            <Button onClick={this.showMergeMemberModal}>
              Merge with another record...
            </Button>
          </div>
          <EditMemberModal
            show={this.state.showEditInfoModal}
            onHide={this.hideEditInfoModal}
            member={memberData}
            onSaveComplete={this.handleSaveComplete}
          />
          <MergeMemberModal
            show={this.state.showMergeMemberModal}
            onHide={this.hideMergeMemberModal}
            member={memberData}
            onSaveComplete={this.handleSaveComplete}
          />
        </section>
      )
    } else {
      return null
    }
  }

  renderCommittees() {
    if (this.state.member == null) {
      return null
    }

    const memberId = this.state.member.get('id')
    const roles = this.state.member
      .get('roles')
      .sortBy(role => role.get('committee'))
      .map((role: ImMemberRole, index) => {
        const roleName = role.get('role')
        const roleCommittee = role.get('committee')
        const roleKey = `role-${index}-${roleName}-${roleCommittee}`
        return (
          <div key={roleKey} className="committee-member">
            <button
              onClick={e => this.showRemoveRolePrompt(roleKey)}
              aria-label={`Remove role ${roleName} from member`}
            >
              <FiTrash />
            </button>
            {`${roleCommittee}: ${roleName}`}
            <Modal
              show={this.state.showRemoveRolePrompt === roleKey}
              onHide={this.hideRemoveRolePrompt}
            >
              <Modal.Header closeButton>
                <Modal.Title>Confirm role removal</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                Are you sure you want to remove the{' '}
                <strong>
                  {roleCommittee} {roleName}
                </strong>{' '}
                role from the member{' '}
                <strong>{this.getNameFromMember(this.state.member!)}</strong>?
              </Modal.Body>
              <Modal.Footer>
                <Button
                  onClick={e => {
                    this.removeRole(memberId, role)
                    this.hideRemoveRolePrompt()
                  }}
                  variant="danger"
                >
                  Remove role
                </Button>
                <Button
                  onClick={this.hideRemoveRolePrompt}
                  variant="outline-secondary"
                >
                  Cancel
                </Button>
              </Modal.Footer>
            </Modal>
          </div>
        )
      })

    return (
      <>
        <section className="member-page-section committees-section">
          <PageHeading level={3}>
            Current {c('GROUP_NAME_PLURAL')} and roles
          </PageHeading>
          {roles.size > 0 ? (
            roles
          ) : (
            <span className="empty-committees list">
              This member is not part of any {c('GROUP_NAME_PLURAL')}.
            </span>
          )}
        </section>
        <AddRole
          memberId={
            this.props.params.memberId != null
              ? parseInt(this.props.params.memberId)
              : this.state.member.get('id')
          }
          refresh={() => this.fetchMemberData()}
        />
      </>
    )
  }

  renderMeetings() {
    if (this.state.member != null) {
      const meetings = this.state.member
        .get('meetings')
        .map((meeting, index) => (
          <div key={`meeting-${index}`}>{meeting.get('name')}</div>
        ))

      return (
        <>
          <section className="meetings-attended-section member-page-section">
            <PageHeading level={3}>Meetings attended</PageHeading>
            {meetings}
          </section>
          <AddMeeting
            memberId={
              this.props.params.memberId != null
                ? parseInt(this.props.params.memberId)
                : this.state.member.get('id')
            }
            refresh={() => this.fetchMemberData()}
          />
          <section className="meeting-code-section member-page-section">
            <PageHeading level={3}>
              Submit attendance using a meeting code
            </PageHeading>
            <Form
              inline
              onSubmit={e => {
                this.attendMeeting(e)
              }}
            >
              <label htmlFor="meetingCode">Meeting Code: #</label>{' '}
              <input
                id="meetingCode"
                type="text"
                placeholder="0000"
                maxLength={4}
                value={this.state.meetingShortId}
                onChange={e => {
                  const meetingCode = e.target.value
                  this.setState({ meetingShortId: meetingCode })
                }}
              />
              <button type="submit">Attend</button>
            </Form>
          </section>
        </>
      )
    } else {
      return null
    }
  }

  renderNotes() {
    const memberData = this.state.member
    if (memberData == null) {
      return null
    }

    const notes = memberData.get('notes')

    return (
      <section className="member-notes-section member-page-section">
        <PageHeading level={3}>Member notes</PageHeading>
        <p className="member-notes-disclaimer">
          Any notes you type here are visible to other officers and co-chairs in
          the chapter, but are currently <strong>not visible</strong> to this
          member or other members.
        </p>
        <MemberNotesTextarea memberId={memberData.get('id')} notes={notes} />
      </section>
    )
  }

  renderEligibleVoters() {
    if (this.state.member != null) {
      return (
        <div>
          <EligibleVotes
            votes={this.state.member.get('votes')}
            elections={this.props.elections.get('byId')}
          />
          <AddEligibleVoter
            memberId={
              this.props.params.memberId != null
                ? parseInt(this.props.params.memberId)
                : this.state.member.get('id')
            }
            refresh={() => this.fetchMemberData()}
          />
        </div>
      )
    } else {
      return null
    }
  }

  renderDuesStatus() {
    const member = this.state.member as ImMemberDetailed
    const status = mapMembershipStatus(member)

    if (status == MS.NotAMember) {
      return <span>N/A</span>
    }

    const membership = member.get('membership') as ImMembership
    const overrideDate = membership?.get(
      'dues_paid_overridden_on'
    ) as Date | null

    const overrideComponent =
      overrideDate != null ? (
        <div>
          <span>
            Dues paid overridden on {overrideDate.toLocaleDateString()}
          </span>
          <OverlayTrigger placement="top-start" overlay={overriddenTooltip}>
            <FiHelpCircle size={24} className="pl-2" />
          </OverlayTrigger>
          <FiXCircle
            size={24}
            className="pl-2 hover"
            onClick={this.cancelDuesOverride}
            color="#D73E0F"
          />
        </div>
      ) : null

    if (status == MS.UnknownStanding) {
      return (
        <div>
          <div>
            <span>Unknown Dues Status</span>
            <OverlayTrigger placement="top-start" overlay={unknownTooltip}>
              <FiHelpCircle size={24} className="pl-2" />
            </OverlayTrigger>
          </div>
          {overrideComponent != null && overrideComponent}
          {overrideComponent == null && <div>Dues have not been confirmed</div>}
        </div>
      )
    } else {
      const duesPaid = member.get('dues_are_paid', false)
      const duesDate = membership.get('dues_paid_until') as Date

      let duesElem
      if (!duesPaid) {
        duesElem = (
          <div className="membership-overridden">
            Dues expired on {duesDate.toLocaleDateString()}
          </div>
        )
      } else {
        duesElem = (
          <div className={overrideDate != null ? 'membership-overridden' : ''}>
            Dues expired on {duesDate.toLocaleDateString()}
          </div>
        )
      }

      return (
        <div>
          {duesElem}
          {overrideComponent != null && overrideComponent}
        </div>
      )
    }
  }

  async fetchMemberData() {
    try {
      this.setState({
        member: await Members.details(this.props.params.memberId)
      })
    } catch (err) {
      return logError('Error loading member details', err)
    }

    if (USE_ELECTIONS) {
      this.props.fetchElections()
    }
  }

  async removeRole(memberId: number, role: ImMemberRole) {
    const committeeId =
      role.get('committee_id') === -1 ? null : role.get('committee_id')

    await Members.removeRole(memberId, role.get('role'), committeeId)
    this.fetchMemberData()
  }

  async attendMeeting(e) {
    e.preventDefault()
    if (this.state.inSubmission || this.state.member == null) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const meeting = await membershipApi(HTTP_POST, '/meeting/attend', {
        meeting_short_id: this.state.meetingShortId,
        member_id: this.props.params.memberId
          ? this.props.params.memberId
          : this.state.member.get('id')
      })
      this.setState({ meetingShortId: '' })
      const landingUrl = fromJS(meeting).get('landing_url', null)
      if (landingUrl) {
        // TODO: Figure out how to inject this for testing
        location.href = landingUrl
      }
      this.fetchMemberData()
    } catch (err) {
      return logError('Error adding attendee', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

interface MembershipStatusProps {
  member: ImMemberDetailed
}

const EligibilityStatus: React.FC<MembershipStatusProps> = ({ member }) => {
  const status = mapMembershipStatus(member)

  return (
    <div>
      <YesNoStatus
        label="Eligible To Vote"
        value={member.get('is_eligible')}
        subItem={false}
      />
      <YesNoStatus
        label="Dues are paid up"
        value={member.get('dues_are_paid')}
        subItem={true}
      />
      <YesNoStatus
        label="Meeting criteria met"
        value={member.get('meets_attendance_criteria')}
        subItem={true}
      />
      <YesNoStatus
        label="Active in committee"
        value={member.get('active_in_committee')}
        subItem={true}
      />
    </div>
  )
}

interface YesNoStatusProp {
  label: string
  value: boolean
  subItem: boolean
}

const YesNoStatus: React.FC<YesNoStatusProp> = ({ label, value, subItem }) => {
  let valueElem
  if (value) {
    valueElem = <span className="yes-text">Yes</span>
  } else {
    valueElem = <span className="no-text">No</span>
  }
  return (
    <div className="yes-no-wrapper">
      <span className={subItem ? 'yes-no-sub-item' : ''}>{label}...</span>
      {valueElem}
    </div>
  )
}

const MembershipStatus: React.FC<MembershipStatusProps> = ({ member }) => {
  const status = mapMembershipStatus(member)

  return <span>{status}</span>
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchElections }, dispatch)

type MemberPageStateProps = RootReducer
type MemberPageDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MemberPageStateProps,
  MemberPageDispatchProps,
  null,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MemberPage)
