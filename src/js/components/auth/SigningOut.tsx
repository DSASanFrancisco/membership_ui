import React from 'react'

const SigningOut: React.FC = () => (
  <div>
    <h1>Signing you out...</h1>
  </div>
)

export default SigningOut
