import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Dispatch, bindActionCreators } from 'redux'
import * as actions from '../../redux/actions/auth'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { c } from 'src/js/config'

interface SigninOwnProps {
  error?: string
}

type SigninProps = SigninOwnProps & SigninDispatchProps & SigninStateProps

class Signin extends Component<SigninProps> {
  container: any

  constructor(props) {
    super(props)
  }

  componentWillMount() {
    const urlParams = new URLSearchParams(window.location.search)
    const redirect = urlParams.get('redirect')
    this.props.clearUserSession()
    this.props.signinUser(redirect)
  }

  componentWillUpdate(nextProps: SigninProps) {
    const { error } = nextProps
    if (error) {
      this.container.error(`${error}`, 'Signin Failed', {
        timeOut: 3000,
        extendedTimeOut: 4000
      })
      nextProps.cleardown()
    }
  }

  handleClick: React.MouseEventHandler<HTMLButtonElement> = () => {
    this.props.signinUser()
  }

  render() {
    return (
      <div>
        <p>Welcome to the {c('CHAPTER_NAME')} member portal. Please sign in.</p>
        <button onClick={this.handleClick}>Sign In</button>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(actions, dispatch)

type SigninStateProps = RootReducer
type SigninDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  SigninStateProps,
  SigninDispatchProps,
  SigninOwnProps,
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(Signin)
