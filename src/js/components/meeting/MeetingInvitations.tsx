import React, { Component } from 'react'
import { List } from 'immutable'
import Loading from 'src/js/components/common/Loading'
import Meetings, {
  ImMeeting,
  ImMeetingInvitation,
  MeetingInvitationStatus
} from 'src/js/client/MeetingClient'
import { logError } from 'src/js/util/util'
import MemberSearchField from '../common/MemberSearchField'
import { EligibleMemberListEntry } from 'src/js/client/MemberClient'
import { Container, Row, Col } from 'react-bootstrap'
import { isGeneralMeeting } from 'src/js/services/meetings'

interface MeetingInvitationsOwnProps {
  meeting: ImMeeting
}
type MeetingInvitationsProps = MeetingInvitationsOwnProps

interface MeetingInvitationsState {
  invitations: List<ImMeetingInvitation> | null
  invitationMember: EligibleMemberListEntry | null
  inSubmission: boolean
}

class MeetingInvitations extends Component<
  MeetingInvitationsProps,
  MeetingInvitationsState
> {
  constructor(props) {
    super(props)
    this.state = {
      invitations: null,
      invitationMember: null,
      inSubmission: false
    }
  }

  componentWillMount() {
    this.fetchInvitations()
  }

  async fetchInvitations() {
    const meetingId = this.props.meeting.get('id')
    try {
      const invitations = await Meetings.getInvitatitons(meetingId)
      this.setState({ invitations })
    } catch (err) {
      return logError(`Error loading invitations for meeting ${meetingId}`, err)
    }
  }

  onMemberSelected(member: EligibleMemberListEntry) {
    this.setState({ invitationMember: member })
  }

  async sendInvitation() {
    if (this.state.inSubmission) {
      return
    }
    const memberId = this.state.invitationMember?.id
    if (memberId === null || memberId === undefined) {
      return
    }
    const meetingId = this.props.meeting.get('id')
    try {
      this.setState({ inSubmission: true })
      const invitation = await Meetings.sendInvitation(meetingId, memberId)
      if (!invitation) {
        return
      }
      this.setState((currentState: MeetingInvitationsState) => ({
        invitations: (currentState.invitations || List()).push(invitation),
        invitationMember: null
      }))
    } catch (err) {
      return logError(
        `Error sending invitation to ${memberId} for meeting ${meetingId}`,
        err
      )
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  async bulkInviteCommitteeMembers() {
    if (this.state.inSubmission) {
      return
    }
    const meetingId = this.props.meeting.get('id')
    try {
      this.setState({ inSubmission: true })
      const invitations = await Meetings.bulkInviteCommitteeMembers(meetingId)
      if (!invitations || !invitations.size) {
        return
      }
      this.setState((currentState: MeetingInvitationsState) => ({
        invitations: (currentState.invitations || List()).concat(invitations)
      }))
    } catch (err) {
      return logError(
        `Error sending bulk invitations for meeting ${meetingId}`,
        err
      )
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  stringForStatus(status: MeetingInvitationStatus): string {
    return {
      SEND_PENDING: 'Pending, will send when meeting published',
      NO_RESPONSE: 'No response',
      ACCEPTED: 'Accepted',
      DECLINED: 'Declined'
    }[status]
  }

  render() {
    const invitations = this.state.invitations
    if (invitations === null) {
      return <Loading />
    }
    return (
      <Container>
        <Row>
          <Col xs={12} style={{ marginTop: '1rem' }}>
            {invitations.size
              ? invitations.map(invitation => (
                  <div
                    key={invitation.get('id')}
                    style={{ marginBottom: '0.5rem' }}
                  >
                    {invitation.get('name')} ({invitation.get('email')}):{' '}
                    {this.stringForStatus(invitation.get('status'))}
                  </div>
                ))
              : 'No invitations sent yet'}
          </Col>
        </Row>
        <hr style={{ margin: '1.5rem 0' }} />
        {isGeneralMeeting(this.props.meeting) ? null : (
          <Row>
            <Col xs={12} style={{ marginBottom: '1rem' }}>
              Bulk invite all committee members:{' '}
              <button
                disabled={this.state.inSubmission}
                onClick={() => this.bulkInviteCommitteeMembers()}
              >
                Send Invites
              </button>
            </Col>
          </Row>
        )}
        <Row>
          <Col xs={12}>
            Send an individual invitation:{' '}
            {this.state.invitationMember ? (
              <div>
                {this.state.invitationMember.name} (
                {this.state.invitationMember.email}){' '}
                <button
                  disabled={this.state.inSubmission}
                  onClick={() => this.sendInvitation()}
                >
                  Send
                </button>
              </div>
            ) : this.state.inSubmission ? (
              <Loading />
            ) : (
              <MemberSearchField
                onMemberSelected={e => this.onMemberSelected(e)}
              />
            )}
          </Col>
        </Row>
      </Container>
    )
  }
}

export default MeetingInvitations
