import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Col, Row } from 'react-bootstrap'
import { Link } from 'react-router'
import { Seq } from 'immutable'
import { Helmet } from 'react-helmet'
import MeetingList from './MeetingList'
import PageHeading from '../common/PageHeading'
import {
  isCommitteeAdmin,
  isAdmin,
  getRelevantCommittees
} from 'src/js/services/members'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { ImMemberState } from 'src/js/redux/reducers/memberReducers'
import { ImMeetingsState } from 'src/js/redux/reducers/meetingReducers'
import { ImMeeting } from '../../client/MeetingClient'

type MeetingsState = {
  committeeFilter: string | null
  ownerFilter: string | null
  sortLatestFirst: boolean
}

class Meetings extends Component<MeetingsPageProps> {
  state = {
    committeeFilter: null,
    ownerFilter: null,
    sortLatestFirst: true
  }

  handleDateSortChange = e => {
    e.preventDefault()
    this.setState((currentState: MeetingsState) => ({
      sortLatestFirst: !currentState.sortLatestFirst
    }))
  }

  renderDateSorter = () => {
    return (
      <div style={{ margin: '0.5rem 1rem' }}>
        <label htmlFor="sort-latest-first" style={{ display: 'block' }}>
          Sort by date
        </label>
        <button
          id="sort-latest-first"
          name="sort-latest-first"
          onClick={this.handleDateSortChange}
        >
          {this.state.sortLatestFirst ? 'Latest first' : 'Oldest first'}
        </button>
      </div>
    )
  }

  handleCommitteeFilterChange = e => {
    e.preventDefault()
    this.setState({ committeeFilter: e.target.value || null })
  }

  renderCommitteeFilter = () => {
    const relevantCommittees = getRelevantCommittees(this.props.member).filter(
      role => role.get('committee_name') !== 'general'
    )
    return (
      <div style={{ margin: '0.5rem 1rem' }}>
        <label htmlFor="committee-filter" style={{ display: 'block' }}>
          Filter by committee
        </label>
        <select
          id="committee-filter"
          name="committee-filter"
          style={{ padding: '5.5px 8px' }}
          onChange={this.handleCommitteeFilterChange}
        >
          <option value="">-</option>
          <option value="general">Chapter</option>
          {relevantCommittees.map(committee => (
            <option
              value={committee.get('committee_id')}
              key={committee.get('committee_id')}
            >
              {committee.get('committee_name')}
            </option>
          ))}
        </select>
      </div>
    )
  }

  handleOwnerFilterChange = e => {
    e.preventDefault()
    this.setState({ ownerFilter: e.target.value || null })
  }

  renderOwnerFilter = () => {
    const meetings = this.props.meetings
      .get('byId')
      .valueSeq()
      .sortBy((meeting: ImMeeting) => -meeting.get('id', 0)) as Seq.Indexed<
      ImMeeting
    >
    const ownerOptions = [
      ...new Set(meetings.map(meeting => meeting.get('owner')))
    ]
    return (
      <div style={{ margin: '0.5rem 1rem' }}>
        <label htmlFor="owner-filter" style={{ display: 'block' }}>
          Filter by owner
        </label>
        <select
          id="owner-filter"
          name="owner-filter"
          style={{ padding: '5.5px 8px' }}
          onChange={this.handleOwnerFilterChange}
        >
          <option value="">-</option>
          {ownerOptions.map(owner => (
            <option value={owner || undefined} key={owner || undefined}>
              {owner}
            </option>
          ))}
        </select>
      </div>
    )
  }

  render() {
    const hasPrivileges =
      isAdmin(this.props.member) || isCommitteeAdmin(this.props.member)

    return (
      <Container>
        <Helmet>
          <title>Meetings</title>
        </Helmet>
        <PageHeading level={2}>Meetings</PageHeading>
        {hasPrivileges && (
          <Row
            style={{
              marginBottom: '1.5rem',
              display: 'flex',
              alignItems: 'flex-end'
            }}
          >
            <Col md={3} style={{ margin: '0.5rem 0' }}>
              <Link to={`/meetings/create`}>
                <button>Create Meeting</button>
              </Link>
            </Col>
            <Col
              md={9}
              style={{ display: 'flex', flexWrap: 'wrap', margin: '0 -1rem' }}
            >
              {this.renderDateSorter()}
              {this.renderCommitteeFilter()}
              {this.renderOwnerFilter()}
            </Col>
          </Row>
        )}
        <MeetingList
          committeeFilter={this.state.committeeFilter}
          ownerFilter={this.state.ownerFilter}
          sortLatestFirst={this.state.sortLatestFirst}
        />
      </Container>
    )
  }
}

const mapStateToProps = (state: RootReducer): MeetingsPageStateProps => ({
  member: state.member,
  meetings: state.meetings
})
interface MeetingsPageStateProps {
  member: ImMemberState
  meetings: ImMeetingsState
}
type MeetingsPageProps = MeetingsPageStateProps

export default connect<MeetingsPageStateProps, {}, null, RootReducer>(
  mapStateToProps
)(Meetings)
