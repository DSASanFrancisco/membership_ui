import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Map } from 'immutable'
import { fetchAllMeetings } from '../../redux/actions/meetingActions'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { Helmet } from 'react-helmet'
import MeetingDetail from './MeetingDetail'
import Loading from 'src/js/components/common/Loading'

interface MeetingDetailParamProps {
  meetingId: string
}
interface MeetingDetailRouteParamProps {}
type MeetingDetailProps = RouteComponentProps<
  MeetingDetailParamProps,
  MeetingDetailRouteParamProps
>

const MeetingDetailPage: React.FC<MeetingDetailProps> = props => {
  const meetings = useSelector((state: RootReducer) => state.meetings)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchAllMeetings())
  }, [])

  if (!meetings || !meetings.size) {
    return <Loading />
  }
  const meeting =
    meetings.getIn(['byId', parseInt(props.params.meetingId, 10)]) || Map()
  const meetingName = meeting.get('name')

  return (
    <>
      <Helmet>
        <title>{meetingName}</title>
      </Helmet>
      <MeetingDetail meetingId={props.params.meetingId} meeting={meeting} />
    </>
  )
}

export default MeetingDetailPage
