import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router'
import { bindActionCreators, Dispatch } from 'redux'
import dateFormat from 'dateformat'
import { isMemberLoaded } from '../../services/members'
import Loading from '../common/Loading'
import { fetchAllMeetings } from '../../redux/actions/meetingActions'
import { Meetings, ImMeetingInvitation } from 'src/js/client/MeetingClient'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { c } from 'src/js/config'
import { ImMeeting } from 'src/js/client/MeetingClient'
import { Helmet } from 'react-helmet'
import { List, Map } from 'immutable'

dateFormat.masks.meetingDetail = 'mmmm d, yyyy h:MM TT'

type MyMeetingsProps = MyMeetingsDispatchProps & MyMeetingsStateProps

interface MyMeetingsState {
  invitations: Map<number, ImMeetingInvitation> | null
  inSubmission: boolean
  showDeclinedMeetings: boolean
}

class MyMeetings extends Component<MyMeetingsProps, MyMeetingsState> {
  constructor(props) {
    super(props)
    this.state = {
      invitations: null,
      inSubmission: false,
      showDeclinedMeetings: false
    }
  }

  componentDidMount() {
    this.props.fetchAllMeetings()
    this.fetchMyInvitations()
  }

  componentDidUpdate(prevProps: MyMeetingsProps) {
    const prevUser = prevProps.member.getIn(['user', 'data'])
    const currentUser = this.props.member.getIn(['user', 'data'])

    if (prevUser !== currentUser) {
      this.props.fetchAllMeetings()
      this.fetchMyInvitations()
    }
  }

  async fetchMyInvitations() {
    const invitations = (await Meetings.getMyInvitations()) || List()
    const invitationsByMeetingId = Map(
      invitations.map((i: ImMeetingInvitation) => [i.get('meeting_id'), i])
    )
    this.setState({ invitations: invitationsByMeetingId })
  }

  async sendRsvp(meetingId, canAttend) {
    this.setState({ inSubmission: true })
    try {
      const invitation = await Meetings.sendRsvp(meetingId, canAttend)

      if (invitation != null) {
        const invitations = (this.state.invitations || Map()).set(
          invitation.get('meeting_id'),
          invitation
        )
        this.setState({ invitations: invitations })
      } else {
        throw new Error("Couldn't send RSVP")
      }
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  render() {
    if (!isMemberLoaded(this.props.member) || this.state.invitations === null) {
      return <Loading />
    }

    return (
      <Container>
        <Helmet>
          <title>My Meetings</title>
        </Helmet>
        <PageHeading level={1}>My Meetings</PageHeading>
        <div style={{ marginBottom: '2rem' }}>
          <h3>What meetings are coming up?</h3>
          {this.renderUpcoming()}
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>What meetings have I been invited to?</h3>
          {this.renderInvitations()}
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>What meetings have I attended?</h3>
          {this.renderAttendance()}
        </div>
        <div style={{ marginBottom: '2rem' }}>
          <h3>Where can I find the rest of the meetings?</h3>
          <p>
            Check out our <Link to={`/meetings`}>full list of meetings</Link>.
          </p>
        </div>
      </Container>
    )
  }

  renderAttendance() {
    const memberData = this.props.member.getIn(['user', 'data'])

    const meetings = memberData
      .get('meetings')
      .reverse()
      .map(meeting => {
        const meetingId = meeting.get('meeting_id')
        const meetingName = meeting.get('name')

        return (
          <div key={`meeting-${meetingId}`}>
            <a href={`meetings/${meetingId}/details`}>{meetingName}</a>
          </div>
        )
      })

    if (meetings.size === 0) {
      return (
        <p>
          You haven't attended any chapter meetings yet. To find our upcoming
          events, check out{' '}
          <a href={c('URL_CHAPTER_EVENTS')}>our events page</a>.
        </p>
      )
    } else {
      return (
        <div>
          <p>You've attended the following meetings so far:</p>
          {meetings}
        </div>
      )
    }
  }

  upcomingMeetings() {
    const memberData = this.props.member.getIn(['user', 'data'])
    const memberRoles = memberData.get('roles')
    return this.props.meetings
      .get('byId')
      .filter((meeting: ImMeeting) => {
        if (!meeting) return false

        // Only show meetings in the future
        const startTime = meeting.get('start_time')
        if (!startTime || startTime.getTime() < Date.now()) {
          return false
        }

        // Only show published meetings
        if (!meeting.get('published')) {
          return false
        }

        // If the user accepted the invite, show it here (declined meetings will be collapsed)
        const hasAcceptedInvite =
          this.state.invitations?.get(meeting.get('id'))?.get('status') ===
          'ACCEPTED'
        if (hasAcceptedInvite) {
          return true
        }
        // If there's an invite but the user has declined or hasn't responded, don't show it here
        const hasUnacceptedInvite =
          this.state.invitations?.get(meeting.get('id')) &&
          this.state.invitations?.get(meeting.get('id'))?.get('status') !==
            'ACCEPTED'
        if (hasUnacceptedInvite) {
          return false
        }

        const committeeId = meeting.get('committee_id')
        // If committee_id is null, this is a general meeting; show it
        if (committeeId === null) {
          return true
        }
        // Otherwise, show the meeting if it's for a committee that the user is a member of
        const forMyCommittees = memberRoles.find(
          role => role.get('committee_id') === committeeId
        )
        if (forMyCommittees) {
          return true
        }

        return false
      })
      .sortBy((meeting: ImMeeting) => meeting.get('start_time'))
  }

  unacceptedInvitesForOtherMeetings() {
    const upcomingMeetingsById = Map(
      this.upcomingMeetings().map((m: ImMeeting) => [m.get('id'), m])
    )
    return this.props.meetings
      .get('byId')
      .filter((meeting: ImMeeting) => {
        if (!meeting) return false

        // Only show meetings in the future
        const startTime = meeting.get('start_time')
        if (!startTime || startTime.getTime() < Date.now()) {
          return false
        }

        // Only show published meetings
        if (!meeting.get('published')) {
          return false
        }

        // If this is shown in the "upcoming meetings" section, don't show it here
        if (upcomingMeetingsById.get(meeting.get('id'))) {
          return false
        }

        // If the user has an invite, respect the provided "withAnInvite" argument
        const hasUnacceptedInvite =
          this.state.invitations?.get(meeting.get('id')) &&
          this.state.invitations?.get(meeting.get('id'))?.get('status') !==
            'ACCEPTED'
        return hasUnacceptedInvite
      })
      .sortBy((meeting: ImMeeting) => meeting.get('start_time'))
  }

  renderInvitations() {
    const unacceptedInvitesForOtherMeetings = this.unacceptedInvitesForOtherMeetings()

    if (unacceptedInvitesForOtherMeetings.size === 0) {
      return <p>You haven't been invited to any other upcoming meetings.</p>
    }
    const noResponseMeetings = unacceptedInvitesForOtherMeetings
      .filter(
        (m: ImMeeting) =>
          this.state.invitations?.get(m.get('id'))?.get('status') ===
          'NO_RESPONSE'
      )
      .toList()
      .map((meeting: ImMeeting) => {
        const meetingId = meeting.get('id')
        const meetingName = meeting.get('name')
        const meetingStartTime = meeting.get('start_time')

        return (
          <div key={`meeting-${meetingId}`} style={{ marginBottom: '1rem' }}>
            <a href={`meetings/${meetingId}/details`}>{meetingName}</a> -{' '}
            {dateFormat(meetingStartTime, 'meetingDetail')}{' '}
            <div style={{ display: 'inline-block' }}>
              <button
                disabled={this.state.inSubmission}
                onClick={() => this.sendRsvp(meetingId, true)}
              >
                Attending
              </button>{' '}
              <button
                disabled={this.state.inSubmission}
                onClick={() => this.sendRsvp(meetingId, false)}
              >
                Not Attending
              </button>
            </div>
          </div>
        )
      })

    const declinedMeetings = unacceptedInvitesForOtherMeetings.filter(
      (m: ImMeeting) =>
        this.state.invitations?.get(m.get('id'))?.get('status') === 'DECLINED'
    )
    const declinedMeetingRows =
      declinedMeetings.size > 0 ? (
        this.state.showDeclinedMeetings ? (
          <div>
            {noResponseMeetings.size > 0 ? (
              <hr style={{ marginTop: '1rem' }} />
            ) : null}
            <button
              onClick={() => this.setState({ showDeclinedMeetings: false })}
              style={{ marginBottom: '1rem' }}
            >
              Hide Declined Meetings
            </button>
            {declinedMeetings.toList().map((meeting: ImMeeting) => {
              const meetingId = meeting.get('id')
              const meetingName = meeting.get('name')
              const meetingStartTime = meeting.get('start_time')

              return (
                <div
                  key={`meeting-${meetingId}`}
                  style={{ marginBottom: '1rem' }}
                >
                  <a href={`meetings/${meetingId}/details`}>{meetingName}</a> -{' '}
                  {dateFormat(meetingStartTime, 'meetingDetail')}{' '}
                  <div style={{ display: 'inline-block' }}>
                    <button
                      disabled={this.state.inSubmission}
                      onClick={() => this.sendRsvp(meetingId, true)}
                    >
                      Change Response to Attending
                    </button>
                  </div>
                </div>
              )
            })}
          </div>
        ) : (
          <button onClick={() => this.setState({ showDeclinedMeetings: true })}>
            Show Declined Meetings
          </button>
        )
      ) : null

    return (
      <>
        {noResponseMeetings}
        {declinedMeetingRows}
      </>
    )
  }

  renderUpcoming() {
    const upcomingMeetings = this.upcomingMeetings()

    if (upcomingMeetings.size === 0) {
      return (
        <p>
          There are no scheduled meetings here. To find our public upcoming
          events, check out{' '}
          <a href={c('URL_CHAPTER_EVENTS')}>our events page</a>.
        </p>
      )
    }

    return upcomingMeetings.toList().map((meeting: ImMeeting) => {
      const meetingId = meeting.get('id')
      const meetingName = meeting.get('name')
      const meetingStartTime = meeting.get('start_time')

      return (
        <div key={`meeting-${meetingId}`}>
          <a href={`meetings/${meetingId}/details`}>{meetingName}</a> -{' '}
          {dateFormat(meetingStartTime, 'meetingDetail')}
        </div>
      )
    })
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ fetchAllMeetings }, dispatch)

type MyMeetingsStateProps = RootReducer
type MyMeetingsDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MyMeetingsStateProps,
  MyMeetingsDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MyMeetings)
