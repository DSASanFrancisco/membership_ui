import React, { useEffect, useState } from 'react'
import {
  Meetings,
  ImMeeting,
  ImMeetingAgenda
} from 'src/js/client/MeetingClient'
import SimpleMarkdownEditor from '../common/SimpleMarkdownEditor'
import { Container, Row, Col } from 'react-bootstrap'
import { logError } from 'src/js/util/util'
import Loading from '../common/Loading'

interface EditMeetingAgendaOwnProps {
  meeting: ImMeeting
}
type EditMeetingAgendaProps = EditMeetingAgendaOwnProps

const EditMeetingAgenda: React.FC<EditMeetingAgendaProps> = props => {
  const [agenda, setAgenda] = useState<ImMeetingAgenda | null>(null)
  const [inSubmission, setInSubmission] = useState(false)

  const getMeetingAgenda = async () => {
    try {
      const agendaResult = await Meetings.getAgenda(props.meeting.get('id'))
      setAgenda(agendaResult)
    } catch (err) {
      return logError(
        `Error loading meeting agenda ${props.meeting.get('id')}`,
        err
      )
    }
  }
  useEffect(() => {
    getMeetingAgenda()
  }, [])

  if (agenda === null) {
    return <Loading />
  }

  const saveMeetingAgenda = async text => {
    try {
      setInSubmission(true)
      const agendaResult = await Meetings.editAgenda(
        props.meeting.get('id'),
        text,
        agenda.get('updated_at')
      )
      setAgenda(agendaResult)
    } catch (err) {
      return console.error(
        `Error editing meeting agenda ${props.meeting.get('id')}`,
        err
      )
    } finally {
      setInSubmission(false)
    }
  }

  return (
    <Container>
      <Row>
        <Col xs={12} lg={{ span: 10, offset: 1 }}>
          <SimpleMarkdownEditor
            initialValue={agenda.get('text')}
            onClickSave={saveMeetingAgenda}
            buttonDisabled={inSubmission}
          />
        </Col>
      </Row>
    </Container>
  )
}

export default EditMeetingAgenda
