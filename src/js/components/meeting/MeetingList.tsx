import React, { Component, useMemo } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Table } from 'react-bootstrap'
import { List, Map, Seq, Set } from 'immutable'
import { Link } from 'react-router'
import { isAdmin, isCommitteeAdmin } from '../../services/members'
import { ImMeeting, Meeting } from '../../client/MeetingClient'
import { fetchCommittees } from '../../redux/actions/committeeActions'
import {
  claimMeetingCode,
  fetchAllMeetings
} from '../../redux/actions/meetingActions'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { Column, useTable } from 'react-table'
import { compact } from 'lodash/fp'
import { find } from 'lodash'
import { ImCommitteesState } from 'src/js/redux/reducers/committeeReducers'
import { ImMemberState } from 'src/js/redux/reducers/memberReducers'

type MeetingListPassedProps = {
  committeeFilter: string | null
  ownerFilter: string | null
  sortLatestFirst: boolean
}

type MeetingListProps = MeetingListDispatchProps &
  MeetingListStateProps &
  MeetingListPassedProps

class MeetingList extends Component<MeetingListProps> {
  componentWillMount() {
    this.props.fetchAllMeetings()
    this.props.fetchCommittees()
  }

  render() {
    const meetings = this.props.meetings
      .get('byId')
      .valueSeq()
      .sortBy((meeting: ImMeeting) => -meeting.get('id', 0)) as Seq.Indexed<
      ImMeeting
    >
    const activeRoles = this.props.member
      .getIn(['user', 'data', 'roles'], List())
      .filter(role => role.get('role') === 'active')
      .map(role => role.get('committee_id'))
      .toSet()
    const filteredMeetings = meetings
      .filter(meeting => {
        const committeeId = meeting.get('committee_id') || 'general'
        const committee =
          this.props.committees.getIn(['byId', committeeId]) || Map()
        const canAdmin =
          isAdmin(this.props.member) ||
          isCommitteeAdmin(this.props.member, committee.get('name'))
        const hasActiveRole =
          committeeId === 'general' ||
          activeRoles.intersect(Set([committeeId])).size
        if (!(canAdmin || hasActiveRole)) {
          return false
        }
        if (
          this.props.committeeFilter &&
          this.props.committeeFilter !== `${committeeId}`
        ) {
          return false
        }
        const owner = meeting.get('owner')
        if (this.props.ownerFilter && this.props.ownerFilter !== owner) {
          return false
        }
        return true
      })
      .sortBy(meeting => meeting.get('start_time'))
    const sortedFilteredMeetings = this.props.sortLatestFirst
      ? filteredMeetings.reverse()
      : filteredMeetings

    return (
      <div>
        {[...sortedFilteredMeetings].length ? (
          <MeetingsTable
            meetings={sortedFilteredMeetings}
            committees={this.props.committees}
            member={this.props.member}
          />
        ) : (
          'No meetings'
        )}
      </div>
    )
  }
}

interface MeetingsTableProps {
  meetings: Seq.Indexed<ImMeeting>
  committees: ImCommitteesState
  member: ImMemberState
}

const MeetingsTable: React.FC<MeetingsTableProps> = ({
  meetings,
  committees,
  member
}) => {
  const memberIsAdmin = isAdmin(member)
  const meetingsAsJs = meetings.toJS()

  const canEditMeeting = meeting => {
    if (memberIsAdmin) {
      return true
    }
    const committeeId = meeting.committee_id
    const committee = committees.getIn(['byId', committeeId]) || Map()
    return isCommitteeAdmin(member, committee.get('name'))
  }
  const canEditAny = find(meetingsAsJs, canEditMeeting)

  const displayProxyLink = meeting =>
    meeting.end_time && new Date() < meeting.end_time
  const displayAnyProxyLinks = find(meetingsAsJs, displayProxyLink)

  const columns = useMemo<Column<Meeting>[]>(
    () =>
      compact([
        {
          Header: 'Name',
          id: 'name',
          accessor: meeting => meeting.name,
          Cell: ({ row: { original } }) => (
            <Link to={`/meetings/${original.id}/details`}>{original.name}</Link>
          )
        },
        {
          Header: 'Time',
          id: 'time',
          accessor: meeting => {
            const startString = meeting.start_time?.toLocaleString(undefined, {
              year: 'numeric',
              month: 'short',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric'
            })
            const startDateString = meeting.start_time?.toDateString()
            const endDateString = meeting.end_time?.toDateString()
            const endOptions = {
              year: startDateString === endDateString ? undefined : 'numeric',
              month: startDateString === endDateString ? undefined : 'short',
              day: startDateString === endDateString ? undefined : 'numeric',
              hour: 'numeric',
              minute: 'numeric'
            }
            const endString = meeting.end_time?.toLocaleString(
              undefined,
              endOptions
            )
            if (!startString) {
              return ''
            } else if (!endString) {
              return startString
            }
            return `${startString} - ${endString}`
          }
        },
        displayAnyProxyLinks && {
          Header: 'Nominate a Proxy',
          id: 'nominate',
          accessor: displayProxyLink,
          Cell: ({ row: { original, values } }) =>
            values.nominate ? (
              <Link to={`/meetings/${original.id}/proxy-token`}>Nominate</Link>
            ) : null
        },
        canEditAny && {
          Header: 'Edit',
          id: 'edit',
          accessor: canEditMeeting,
          Cell: ({ row: { original, values } }) =>
            values.edit ? (
              <Link to={`/meetings/${original.id}/admin`}>Admin</Link>
            ) : null
        }
      ]),
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = useTable({
    columns,
    data: meetings.toJS()
  })

  return (
    <Table
      striped
      bordered
      hover
      {...getTableProps()}
      className="meetings-table"
    >
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>{column.render('Header')}</th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      claimMeetingCode,
      fetchAllMeetings,
      fetchCommittees
    },
    dispatch
  )

type MeetingListStateProps = RootReducer
type MeetingListDispatchProps = ReturnType<typeof mapDispatchToProps>

export default connect<
  MeetingListStateProps,
  MeetingListDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(MeetingList)
