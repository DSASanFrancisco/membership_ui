import { capitalize, compact } from 'lodash/fp'
import React, { Component } from 'react'
import { Nav, Navbar, NavbarBrand } from 'react-bootstrap'
import { connect } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { bindActionCreators, Dispatch } from 'redux'
import { NavLinkEntry } from 'src/js/components/admin/Admin'
import { c, USE_ELECTIONS } from 'src/js/config'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import logo from '../../../../images/dsa-logo.png'
import { fetchMember } from '../../redux/actions/memberActions'
import { isAdmin } from '../../services/members'

const navMap: NavLinkEntry[] = compact([
  {
    link: '/home',
    label: 'Home'
  },
  {
    link: '/my-membership',
    label: 'Membership'
  },
  USE_ELECTIONS && {
    link: '/my-elections',
    label: 'Elections'
  },
  {
    link: '/my-committees',
    label: capitalize(c('GROUP_NAME_PLURAL'))
  },
  {
    link: '/my-meetings',
    label: 'Meetings'
  },
  {
    link: '/my-contact-info',
    label: 'Contact Info'
  },
  {
    link: '/my-resources',
    label: 'Resources'
  }
])

type NavigationProps = NavigationStateProps & NavigationDispatchProps

class Navigation extends Component<NavigationProps> {
  componentWillReceiveProps(nextProps) {
    if (localStorage == null) {
      return
    }
    const authUser = localStorage.user
    if (authUser == null || authUser.email == null) {
      return
    }
    const dbMember = nextProps.member.getIn(['user', 'data'], null)
    if (
      (dbMember === null || dbMember.get('email_address') !== authUser.email) &&
      !nextProps.member.getIn(['user', 'loading']) &&
      nextProps.member.getIn(['user', 'error']) === null
    ) {
      this.props.fetchMember()
    }
  }

  render() {
    const links = navMap.map(({ link, label }) => (
      <Nav.Item key={link} className="dsaNavItem">
        {link != null ? (
          <LinkContainer to={link}>
            <Nav.Link>{label}</Nav.Link>
          </LinkContainer>
        ) : (
          <Nav.Link>{label}</Nav.Link>
        )}
      </Nav.Item>
    ))
    return (
      <Navbar collapseOnSelect tabIndex={-1} expand="lg" className="mainNav">
        <NavbarBrand href="/home">
          <img
            src={logo}
            alt=""
            className="d-inline-block align-top"
            style={{ maxHeight: '55px' }}
          />
        </NavbarBrand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse>
          <Nav className="mr-auto">
            {links}
            {isAdmin(this.props.member) && (
              <Nav.Item key="admin-link" className="dsaNavItem">
                <LinkContainer to="/admin">
                  <Nav.Link>Admin</Nav.Link>
                </LinkContainer>
              </Nav.Item>
            )}
          </Nav>
          <Nav>
            <Nav.Item className="dsaNavItem">
              <LinkContainer to="/logout">
                <Nav.Link>Logout</Nav.Link>
              </LinkContainer>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      fetchMember
    },
    dispatch
  )

type NavigationDispatchProps = ReturnType<typeof mapDispatchToProps>
type NavigationStateProps = RootReducer

export default connect<
  NavigationStateProps,
  NavigationDispatchProps,
  {},
  RootReducer
>(
  state => state,
  mapDispatchToProps
)(Navigation)
