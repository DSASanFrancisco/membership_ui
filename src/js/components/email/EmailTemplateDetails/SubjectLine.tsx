import React from 'react'

type SubjectLineProps = {
  subject: string
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void
}

const SubjectLine: React.FC<SubjectLineProps> = ({ subject, onChange }) => {
  return (
    <>
      <label htmlFor="subject">Subject</label>
      <br />
      <textarea
        id="subject"
        name="subject"
        rows={1}
        cols={20}
        value={subject}
        onChange={onChange}
      />
    </>
  )
}

export default SubjectLine
