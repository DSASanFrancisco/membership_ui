import React from 'react'
import {
  EmailById,
  ImEmailsListResponse,
  ImEmail
} from 'src/js/client/EmailAddressClient'

type FromSelectorProps = {
  addresses: ImEmailsListResponse
  currentAddress: string
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
}

const FromSelector: React.FC<FromSelectorProps> = ({
  addresses,
  currentAddress,
  onChange
}) => {
  const emails = addresses.map((address: ImEmail) => {
    const email = address.get('email_address')
    return (
      <option key={email} value={email}>
        {email}
      </option>
    )
  })
  return (
    <>
      <label htmlFor="emailsSelect">Send From:</label>
      <select
        id="emailsSelect"
        style={{ marginLeft: 10 }}
        value={currentAddress}
        onChange={onChange}
      >
        {emails}
      </select>
    </>
  )
}

export default FromSelector
