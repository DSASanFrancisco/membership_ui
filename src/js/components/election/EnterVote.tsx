import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Container, Form } from 'react-bootstrap'
import FieldGroup from '../common/FieldGroup'
import PaperBallot from './PaperBallot'
import { fromJS, List, Map, Range } from 'immutable'
import {
  ElectionDetailsResponse,
  Elections,
  ImGetVoteResponse,
  ImPaperBallotRequest
} from '../../client/ElectionClient'
import PageHeading from '../common/PageHeading'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { ImCandidate } from 'src/js/client/CandidateClient'
import { FromJS } from 'src/js/util/typedMap'
import {
  sanitizeBallotKey,
  sanitizeIntermediateBallotInput
} from 'src/js/util/ballotKey'
import { Helmet } from 'react-helmet'

interface ValidationMessage {
  status: 'info' | 'warning' | 'success' | 'error'
  message: JSX.Element
}

interface EnterVoteParamProps {
  electionId: string
}
interface EnterVoteRouteParamProps {}
type EnterVoteStateProps = RootReducer
type EnterVoteProps = EnterVoteStateProps &
  RouteComponentProps<EnterVoteParamProps, EnterVoteRouteParamProps>

interface ElectionState extends ElectionDetailsResponse {
  candidatesById?: { [key: number]: ImCandidate }
}
type ImElectionState = FromJS<ElectionState>

interface EnterVoteState {
  ballotKeyInput: string
  searchingForVote: boolean
  existingVote: ImGetVoteResponse | null
  vote: ImPaperBallotRequest | null
  rankings: Map<number, number>
  sortedCandidates: number[]
  submitMessage: null
  election: ImElectionState | null
  validation: ValidationMessage | null
  inSubmission: boolean
}

class EnterVote extends Component<EnterVoteProps, EnterVoteState> {
  static initValidationMessage: ValidationMessage = {
    status: 'info',
    message: (
      <p>
        <strong>Enter a 5 digit ballot key to proceed.</strong>
      </p>
    )
  }

  static blankBallot(electionId: number, ballotKey: string): ImGetVoteResponse {
    return fromJS({
      election_id: electionId,
      ballot_key: ballotKey,
      rankings: [] as number[]
    })
  }

  searchBallotKeyRef = React.createRef<FieldGroup<'searchBallotKey'>>()

  constructor(props) {
    super(props)
    this.state = {
      ballotKeyInput: '',
      searchingForVote: false,
      existingVote: null,
      vote: null,
      sortedCandidates: [],
      rankings: Map(),
      submitMessage: null,
      election: null,
      validation: EnterVote.initValidationMessage,
      inSubmission: false
    }
  }

  componentWillMount() {
    Elections.getElection(parseInt(this.props.params.electionId)).then(
      election => {
        if (election != null) {
          const candidatesById: Map<number, ImCandidate> = election
            .get('candidates', List())
            .toMap()
            .mapEntries(([idx, c]) => [c.get('id'), c])
          const sortedCandidates = candidatesById
            .keySeq()
            .toArray()
            .sort()
          this.setState({
            election: (election as ImElectionState).set(
              'candidatesById',
              candidatesById
            ) as ImElectionState,
            sortedCandidates
          })
        }
      }
    )
    const ballotKey = sanitizeBallotKey(this.state.ballotKeyInput)
    if (ballotKey != null) {
      this.searchForVote(ballotKey).then(() => {
        this.setState({ vote: this.state.existingVote })
      })
    }
  }

  async searchForVote(ballotKey: string) {
    this.setState({ searchingForVote: true })
    let existingVote: ImGetVoteResponse | null = null
    try {
      existingVote = await Elections.getVote(
        parseInt(this.props.params.electionId),
        ballotKey
      )
    } finally {
      this.setState({
        existingVote,
        searchingForVote: false,
        validation: existingVote
          ? null
          : {
              status: 'warning',
              message: (
                <p>
                  <strong>{`Ballot #${ballotKey} is unclaimed.`}</strong>
                  You must claim it before you can submit this vote
                </p>
              )
            }
      })
    }
  }

  static sortRankings(rankings: Map<number, number>): number[] {
    return rankings
      .entrySeq()
      .sortBy(([cid, rank]) => rank)
      .map(([cid, rank]) => cid)
      .toArray()
  }

  verifyOrSubmit: React.MouseEventHandler<HTMLButtonElement> = async event => {
    if (
      this.state.existingVote &&
      !this.state.existingVote.get('rankings', List()).isEmpty()
    ) {
      this.verifyVote()
    } else if (this.state.vote != null) {
      const ballotKey = this.state.vote.get('ballot_key')
      const resp = confirm(
        `Are you sure you want to submit ballot #${ballotKey}?`
      )
      if (resp) {
        const rankedCandidates = EnterVote.sortRankings(this.state.rankings)
        const voteBody = this.state.vote.set('rankings', List(rankedCandidates))
        await this.submitVoteAndReset(event, () =>
          Elections.submitPaperBallot(voteBody)
        )
      }
    }
  }

  verifyVote = () => {
    if (this.state.vote == null || this.state.existingVote == null) {
      return
    }

    const rankedCandidates = EnterVote.sortRankings(this.state.rankings)
    const existingRankedCandidates = this.state.existingVote.get(
      'rankings',
      List()
    )
    if (List(rankedCandidates).equals(existingRankedCandidates)) {
      this.setState({
        sortedCandidates: rankedCandidates,
        validation: {
          status: 'success',
          message: (
            <p>
              <strong>Vote matches previously submitted ballot!</strong>
            </p>
          )
        }
      })
    } else {
      this.setState({
        sortedCandidates: rankedCandidates,
        validation: {
          status: 'error',
          message: (
            <p>
              <strong>
                Vote does not match order from previously submitted ballot!
              </strong>
            </p>
          )
        }
      })
    }
  }

  handleOverrideBallot: React.MouseEventHandler<HTMLButtonElement> = e => {
    if (this.state.vote != null) {
      const ballotKey = this.state.vote.get('ballot_key')
      const resp = confirm(
        `Are you sure you want to overwrite ballot #${ballotKey}?`
      )
      if (resp) {
        const rankedCandidates = EnterVote.sortRankings(this.state.rankings)
        const voteBody = this.state.vote.set('rankings', List(rankedCandidates))
        this.submitVoteAndReset(e, () =>
          Elections.submitPaperBallot(voteBody, true).then(() => {
            this.setState({
              validation: {
                status: 'success',
                message: (
                  <p>
                    <strong>{`Ballot #${ballotKey} updated successfully!`}</strong>
                  </p>
                )
              }
            })
          })
        )
      }
    }
  }

  handleSearchBallotKey = (formKey: string, value: string) => {
    const searchBallotKey = sanitizeBallotKey(value)

    if (searchBallotKey != null) {
      this.searchForVote(searchBallotKey)
      this.setState({
        ballotKeyInput: searchBallotKey,
        vote: EnterVote.blankBallot(
          parseInt(this.props.params.electionId),
          searchBallotKey
        )
      })
    } else {
      const input = sanitizeIntermediateBallotInput(value)

      if (input != null) {
        this.setState({ vote: null, ballotKeyInput: input })
      }
    }

    this.setState({ validation: EnterVote.initValidationMessage })
  }

  render() {
    if (this.state.election === null) {
      return (
        <div className="no-election">
          Election doesn't exist in state (not loaded yet?)
        </div>
      )
    }

    if (this.state.sortedCandidates == null) {
      return <div className="no-candidates">No candid ates (not loaded?)</div>
    }

    const showBallot =
      !this.state.searchingForVote &&
      this.state.vote != null &&
      this.state.existingVote != null

    const ballot = showBallot ? (
      <PaperBallot
        editable={true}
        election={this.state.election}
        ballotKey={this.state.vote!.get('ballot_key')}
        onRankingChange={rankings => {
          const sortedCandidates = EnterVote.sortRankings(rankings)
          this.setState({
            rankings,
            sortedCandidates,
            validation: null
          })
        }}
      />
    ) : null

    let validationBox: JSX.Element | null = null
    let overrideButton: JSX.Element | null = null
    let validationTable: JSX.Element | null = null
    if (this.state.validation) {
      // We have a validation box to show
      const status = this.state.validation.status
      if (status === 'error') {
        const existingRankings =
          this.state.existingVote?.get('rankings', List()).toArray() || []
        const currentRankings = this.state.sortedCandidates
        const totalRange = Range(
          0,
          Math.max(existingRankings.length, currentRankings.length)
        )
        validationTable = (
          <table>
            <thead>
              <tr>
                <th>Existing Ballot</th>
                <th>Current Ballot</th>
                <th>Matching?</th>
              </tr>
            </thead>
            <tbody>
              {totalRange.toSeq().map(i => {
                if (this.state.election == null) {
                  return (
                    <tr key={i}>
                      <td>Election is null</td>
                    </tr>
                  )
                } else {
                  const existing = this.state.election.getIn([
                    'candidatesById',
                    existingRankings[i],
                    'name'
                  ])
                  const current = this.state.election.getIn([
                    'candidatesById',
                    currentRankings[i],
                    'name'
                  ])
                  return (
                    <tr key={`validation-rank-comparison-${i}`}>
                      <td>{existing}</td>
                      <td>{current}</td>
                      <td>{existing === current ? '√' : 'X'}</td>
                    </tr>
                  )
                }
              })}
            </tbody>
          </table>
        )
        // If we are displaying an error with the ballot, we have the option to override the ballot to submit the current one
        overrideButton = (
          <button className="center-block" onClick={this.handleOverrideBallot}>
            Overwrite Ballot
          </button>
        )
      }
      const validationClasses = [
        'validation-box',
        'center-block',
        'center-text',
        'alert'
      ]
      switch (this.state.validation.status) {
        case 'success':
          validationClasses.push('alert-success')
          break
        case 'info':
          validationClasses.push('alert-info')
          break
        case 'warning':
          validationClasses.push('alert-warning')
          break
        case 'error':
          validationClasses.push('alert-danger')
          break
        default:
      }
      validationBox = (
        <div className={validationClasses.join(' ')}>
          <span className="text-center">{this.state.validation.message}</span>
          <div className="text-center">{validationTable}</div>
          {overrideButton}
        </div>
      )
    }

    const pageTitle = `Enter Ballot for ${this.state.election.get('name')}`

    return (
      <Container>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <PageHeading level={1}>{pageTitle}</PageHeading>
        <Form onSubmit={e => e.preventDefault()}>
          <FieldGroup
            required
            formKey="searchBallotKey"
            id="searchBallotKey"
            ref={this.searchBallotKeyRef}
            componentClass="input"
            type="text"
            label="Ballot Key"
            maxLength={5}
            value={this.state.ballotKeyInput}
            onFormValueChange={this.handleSearchBallotKey}
          />
          {validationBox}
          {ballot ? (
            <div>
              {ballot}
              <button id="verify" onClick={e => this.verifyOrSubmit(e)}>
                {this.state.rankings.isEmpty() ? 'Submit Vote' : 'Verify Vote'}
              </button>
            </div>
          ) : null}
        </Form>
      </Container>
    )
  }

  async submitVoteAndReset(
    e: React.MouseEvent<HTMLButtonElement>,
    submitFn: () => Promise<unknown>
  ): Promise<void> {
    await this.submitForm(e, submitFn)
    if (this.state.vote != null) {
      await this.searchForVote(this.state.vote.get('ballot_key'))
    }
  }

  async submitForm<T>(e, call: () => Promise<unknown>): Promise<void> {
    e.preventDefault()
    if (this.state.inSubmission) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      await call()
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<EnterVoteStateProps, null, null, RootReducer>(
  state => state
)(EnterVote)
