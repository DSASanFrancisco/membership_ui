import React, { Component } from 'react'
import { connect } from 'react-redux'
import DateTime from 'react-datetime'
import { isAdmin } from '../../services/members'
import {
  Elections,
  UpdateElectionAttributes,
  ElectionDetailsResponse,
  ImElectionDetailsResponse
} from '../../client/ElectionClient'
import { ImCandidate } from '../../client/CandidateClient'
import { logError } from '../../util/util'
import FieldGroup from '../common/FieldGroup'
import { Col, Form, Row, Container } from 'react-bootstrap'
import { fromJS, List, Map } from 'immutable'
import CandidateDisplay from './Candidate'
import PageHeading from '../common/PageHeading'
import CandidateEdit from './CandidateEdit'
import { RootReducer } from 'src/js/redux/reducers/rootReducer'
import { RouteComponentProps } from 'react-router'
import { mapValues, pickBy, compact } from 'lodash'
import { Helmet } from 'react-helmet'

interface ElectionEditParamProps {
  electionId: string
}
interface ElectionEditRouteParamProps {}
type ElectionEditStateProps = RootReducer
type ElectionEditOtherProps = RouteComponentProps<
  ElectionEditParamProps,
  ElectionEditRouteParamProps
>
type ElectionEditProps = ElectionEditStateProps & ElectionEditOtherProps

interface ElectionEditState {
  election: ImElectionDetailsResponse | null
  inSubmission: boolean
}

type ElectionDetailsUpdateAttributes = Pick<
  ElectionDetailsResponse,
  'voting_begins_epoch_millis' | 'voting_ends_epoch_millis' | 'number_winners'
>

class ElectionEdit extends Component<ElectionEditProps, ElectionEditState> {
  constructor(props) {
    super(props)
    this.state = {
      election: null,
      inSubmission: false
    }
  }

  componentDidMount() {
    this.getElectionDetails()
  }

  updateStartTime = (startTime: Date) => {
    if (this.state.inSubmission || this.state.election == null) {
      return
    }

    this.setState({
      election: this.state.election.set(
        'voting_begins_epoch_millis',
        new Date(startTime).getTime()
      )
    })
  }

  updateEndTime = (endTime: Date) => {
    if (this.state.inSubmission || this.state.election == null) {
      return
    }

    this.setState({
      election: this.state.election.set(
        'voting_ends_epoch_millis',
        new Date(endTime).getTime()
      )
    })
  }

  updateNumberOfWinners = (numberOfWinners: number) => {
    if (this.state.inSubmission || this.state.election == null) {
      return
    }

    this.setState({
      election: this.state.election.set('number_winners', numberOfWinners)
    })
  }

  updateElection: React.MouseEventHandler<HTMLButtonElement> = async e => {
    e.preventDefault()

    if (this.state.inSubmission || this.state.election == null) {
      return
    }
    this.setState({
      inSubmission: true
    })

    try {
      const attributeNames = [
        'voting_begins_epoch_millis',
        'voting_ends_epoch_millis',
        'number_winners'
      ]
      const filteredAttributes = this.state.election
        // filter out attribute names we don't want to send up
        .filter((_, key) => attributeNames.includes(key))
        // treat falsy values as null when sending up
        .map((val, _) => val || null)

      const updatedAttributes = this.cleanupDates(filteredAttributes)

      const result = await Elections.updateElection(
        parseInt(this.props.params.electionId),
        fromJS(updatedAttributes)
      )
      // parse received ISO 8601 strings
      const election = result.get('election')
      const updatedElection = this.state.election.merge(
        election
      ) as ImElectionDetailsResponse
      this.setState({ election: updatedElection })
    } finally {
      this.setState({ inSubmission: false })
    }
  }

  cleanupDates = updatedAttributes => {
    const startTime = updatedAttributes.get('voting_begins_epoch_millis')
    const endTime = updatedAttributes.get('voting_ends_epoch_millis')
    if (startTime) {
      // rename attribute & convert for api requirements
      updatedAttributes = updatedAttributes.set(
        'start_time',
        new Date(startTime).toISOString()
      )
    }
    if (endTime) {
      // rename attribute & convert for api requirements
      updatedAttributes = updatedAttributes.set(
        'end_time',
        new Date(endTime).toISOString()
      )
    }
    return updatedAttributes
  }

  onCreateCandidate = (newCandidate: ImCandidate) => {
    if (this.state.election != null) {
      this.setState({
        election: this.state.election.set(
          'candidates',
          this.state.election.get('candidates').push(newCandidate)
        )
      })
    }
  }

  onDeleteCandidate = (deletedCandidateId: number) => {
    if (this.state.election != null) {
      const candidatesAfter = this.state.election
        .get('candidates')
        .filter(candidate => candidate.get('id') !== deletedCandidateId)
      this.setState({
        election: this.state.election.set('candidates', candidatesAfter)
      })
    }
  }

  render() {
    const admin = isAdmin(this.props.member)
    if (!admin || this.state.election == null) {
      return <div />
    }

    const status = this.state.election.get('status')
    const canEdit = status === 'draft'
    const existingCandidates = this.state.election.get('candidates')

    const candidateElements = this.renderCandidates(
      this.state.election.get('id'),
      existingCandidates,
      canEdit
    )

    const transitions = this.state.election
      .get('transitions', List())
      .map(t => (
        <li
          key={t}
          style={{
            display: 'inline-block',
            marginRight: 10
          }}
        >
          <button
            type="submit"
            value={t}
            onClick={e => this.submitTransition(e)}
          >
            {t}
          </button>
        </li>
      ))

    return (
      <Container>
        <Helmet>
          <title>{this.state.election.get('name')}</title>
        </Helmet>
        <Row>
          <Col sm={8} lg={6}>
            <PageHeading level={1}>
              {this.state.election.get('name')} ({status})
            </PageHeading>
          </Col>
        </Row>
        <Row>
          <Col sm={8} lg={6}>
            <h3>{canEdit ? 'Edit Details' : 'Details'}</h3>
            <Form onSubmit={e => e.preventDefault()}>
              <FieldGroup
                formKey="start_time"
                componentClass="datetime"
                label="Start Time"
                value={
                  new Date(
                    this.state.election.get('voting_begins_epoch_millis')
                  )
                }
                onFormValueChange={(_, value) => this.updateStartTime(value)}
                disabled={!canEdit}
              />
              <FieldGroup
                formKey="end_time"
                componentClass="datetime"
                label="End Time"
                value={
                  new Date(this.state.election.get('voting_ends_epoch_millis'))
                }
                onFormValueChange={(_, value) => this.updateEndTime(value)}
                disabled={!canEdit}
              />
              <FieldGroup
                formKey="landingUrl"
                componentClass="input"
                type="number"
                label="Number of Positions"
                value={this.state.election.get('number_winners')}
                onFormValueChange={(_, value) =>
                  this.updateNumberOfWinners(value)
                }
                disabled={!canEdit}
              />
              {canEdit ? (
                <button type="submit" onClick={this.updateElection}>
                  Submit
                </button>
              ) : null}
            </Form>
          </Col>
        </Row>
        <Row>
          <Col sm={8} lg={6}>
            <h3>{canEdit ? 'Edit Candidates' : 'Candidates'}</h3>
            {candidateElements}
          </Col>
        </Row>
        <Row>
          <Col sm={8} lg={6}>
            <h3>Update Election Status</h3>
            <ul
              style={{
                listStyleType: 'none',
                paddingLeft: 0
              }}
            >
              {transitions}
            </ul>
          </Col>
        </Row>
      </Container>
    )
  }

  renderCandidates = (
    electionId: number,
    existingCandidates: List<ImCandidate>,
    canEdit: boolean
  ): JSX.Element[] => {
    const existingCandidateEntries = existingCandidates.map(candidate =>
      canEdit ? (
        <CandidateEdit
          key={candidate.get('id') || 'new'}
          imageUrl={candidate.get('image_url')}
          name={candidate.get('name')}
          electionId={electionId}
          candidateId={candidate.get('id')}
          onCreate={this.onCreateCandidate}
          onDelete={() => this.onDeleteCandidate(candidate.get('id'))}
        />
      ) : (
        <CandidateDisplay
          key={candidate.get('id')}
          imageUrl={candidate.get('image_url')}
          name={candidate.get('name')}
        />
      )
    )

    return compact([
      ...existingCandidateEntries,
      canEdit ? (
        <CandidateEdit
          key="new-candidate"
          electionId={electionId}
          onCreate={this.onCreateCandidate}
        />
      ) : null
    ])
  }

  async getElectionDetails() {
    try {
      const results = await Elections.getElection(
        parseInt(this.props.params.electionId)
      )
      this.setState({ election: results })
    } catch (err) {
      return logError('Error fetching election', err)
    }
  }

  async submitTransition(e) {
    e.preventDefault()
    if (this.state.inSubmission || this.state.election == null) {
      return
    }
    const transition = e.target.value
    const confirmation = confirm(
      `Are you sure you want to ${transition} the election?`
    )
    if (!confirmation) {
      return
    }
    this.setState({ inSubmission: true })
    try {
      const results = await Elections.submitTransition(
        parseInt(this.props.params.electionId),
        transition
      )
      const newElection = this.state.election
        .set('status', results.get('election_status'))
        .set('transitions', results.get('transitions'))

      this.setState({ election: newElection })
    } catch (err) {
      return logError('Error changing election state', err)
    } finally {
      this.setState({ inSubmission: false })
    }
  }
}

export default connect<ElectionEditStateProps, null, null, RootReducer>(
  state => state
)(ElectionEdit)
